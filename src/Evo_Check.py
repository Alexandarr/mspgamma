import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import quad, nquad
import scipy.special as special
plt.style.use('classic')
# all masses in msun, lengths in kpc

halo=118
# Input #############################################################################  
GCini=np.loadtxt('Fortran/GCini_Hui_Halo%i.txt'%halo)
GCini[:,6]=10**GCini[:,6]
GCini=GCini[GCini[:,9].argsort()]
#0 haloID |1 logMh(z=0) |2 haloID @ form |3 logMh(tform) |4 logM*(tform) |5 logMgas(tform) |6 logMcl(tform) |7 zform |8 [Fe/H] |9 rGalaxy (kpc) 
GCfin=np.loadtxt('Fortran/GCfin_Hui_Halo%i.dat'%halo)
actfin=GCfin[GCfin[:,1]==1]
#[0 GC ind, 1 status ind, 2 m_fin(msun), 3 m_ini, 4 t_fin_fromnow(gyr), 5 t_ini, 6 r_fin(kpc), 7 r_ini]
#status ind = 1-alive; -1-exhausted; -2-torn; -3-sunk into galaxy center
Debris=np.loadtxt('Fortran/Depos_Hui_Halo%i.dat'%halo)
#debris: [0 t_fromnow(gyr), 1 bin#, 2 r_in(kpc), 3 r_out, 4 m_depo(msun), 5 m* no evo, 6 m* wind loss, 7 lum_c(erg/s), 8 lum_eq]
timestep=int(len(Debris)/100)
###################################################################################################
#GC initial distributions at z=4
#MW sersic
fGC_i = 0.012  #mass fraction of GCs         
Re=4
n=2.2
mstar_tot=5e10

def sersic_star_tot(A):
    return 4*np.pi*A*Re**3*n*(2*n-1/3+0.009876/n)**(n*(-2-0.6097/n+0.05563/n**2))*special.gamma(n*(2+0.6097/n-0.05563/n**2))
A = mstar_tot/sersic_star_tot(1)

def rho_sersic_GC(r):
    return fGC_i* A * (Re/r)**(1-0.6097/n+0.05563/n**2) * np.exp( -(2*n-1/3+0.009876/n) * (r/Re)**(1./n) )
def rho_sersic_GC_itg(r): #mass integrand
    return rho_sersic_GC(r)*4*np.pi*r**2

#M31 uses a bulge+disk model
fGC_i=0.0075

M_bulge=1.9e10
a=1
def rho_bulge_GC(r):
    return fGC_i*M_bulge/2/np.pi*a/r/(r+a)**3 
def rho_bulge_GC_itg(r):
    return rho_bulge_GC(r)*4*np.pi*r**2

M_disk=8e10
b=5
c=1
def R(r,theta):
    return r*np.sin(theta)
def z(r,theta):
    return r*np.cos(theta)
def rho_disk_GC(r,theta):
    return fGC_i*c**2*M_disk/4/np.pi * (b*R(r,theta)**2 +(b+3*np.sqrt(z(r,theta)**2+c**2))*(b+np.sqrt(z(r,theta)**2+c**2))**2) / (R(r,theta)**2+(b+np.sqrt(z(r,theta)**2+c**2))**2)**2.5 / (z(r,theta)**2+c**2)**1.5    
def rho_disk_GC_itg(r,theta):
    return rho_disk_GC(r,theta)*r**2*np.sin(theta)*2*np.pi

###################### check mass conservation ################################
print('Halo%i mass = '%halo,10**GCini[0,1],' msun')
print('Initial GC mass = ',sum(GCini[:,6]))
print('Final GC+depost = ',sum(GCfin[:,2])+sum(Debris[100*(timestep-1):,4]))
print('Final GC mass =',sum(actfin[:,2]))
print('Deposit mass =',sum(Debris[100*(timestep-1):,4]))
print('Mgc/Mhalo = ',sum(actfin[:,2])/10**GCini[0,1])

###################### check number density ###################################
'''
def ininumbdens(r):
    return rho_sersic_GC(r)/6.91/10**4
r=np.logspace(-1,2)
plt.plot(r,ininumbdens(r), 'k:',label='Initial GCs Analytical')
'''
ir=0.01
jGCi=0
jGCf=0
mtpl=1.3
r=[]
deni=[]
denf=[]
while ir<150 :
    numbi=0
    numbf=0   
    while jGCi<len(GCini) and GCini[jGCi,9]<ir:
        numbi+=1
        jGCi+=1
    while jGCf<len(actfin) and actfin[jGCf,6]<ir:
        numbf+=1
        jGCf+=1
        
    r.append(ir)
    vol=4/3*np.pi*(ir**3-(ir/mtpl)**3)
    deni.append(numbi/vol)
    denf.append(numbf/vol)
    ir*=mtpl
r.append(r[-1])
deni.append(0)
denf.append(0)

plt.plot(r,deni,'k',label='Initial GCs')
plt.plot(r,denf,'r',label='Survival GCs')
plt.legend(loc='upper right')
plt.xscale('log')
plt.xlim(0.1,100)
plt.yscale('log') 
plt.ylim(10**(-7),100)
plt.xlabel(r'$r \; (\rm kpc)$')
plt.ylabel(r'$\rm GC \; density \; (kpc^{-3})$')
plt.title('GC Number Density for Halo%i'%halo)
plt.savefig('pics/Halo Running/NumDens%i.png'%halo)
plt.show()

################## check mass function ########################################
M1range=np.logspace(4,8,17)
M2range=np.logspace(2,7,21)
plt.hist(GCini[:,6],bins=M1range,histtype='step',color='b',label='Initial')
plt.hist(actfin[:,2],bins=M2range,histtype='step',color='r',label='Final')
plt.legend()
plt.xscale('log')
plt.xlim(10**2,2.5*10**7)
plt.yscale('log')
plt.ylim(0.5,10**4)
plt.xlabel(r'$M_{\rm GC} \; (M_\odot)$')
plt.ylabel(r'$N$')
plt.title('GC Mass Function for Halo%i'%halo)
plt.savefig('pics/Halo Running/MasFunc%i.png'%halo)
plt.show()

################ check cumulative mass spatial distribution ###################
'''
i=0.01
j=1
mtpl=1.2
x=[0]
sersic=[0]
bd=[0]
ops={'epsrel':1e-02}
while i<200: 
    serincr=quad(rho_sersic_GC_itg,i/mtpl,i,epsrel=1e-02)[0]
    bdincr=quad(rho_bulge_GC_itg,i/mtpl,i,epsrel=1e-02)[0] + nquad(rho_disk_GC_itg,[[i/mtpl,i],[0,np.pi]],opts=[ops,ops])[0]
    x.append(i)
    sersic.append(sersic[j-1]+serincr)
    bd.append(bd[j-1]+bdincr)
    i*=mtpl
    j+=1

plt.plot(x,sersic,'k--',label='Initial Sersic at z=4') 
#plt.plot(x,bd,'k--',label='Initial BD at z=4')     
'''
inir=np.append(GCini[:,9],100)
inim=np.append(GCini[:,6],0)
plt.plot(inir,np.cumsum(inim),'k',label='Initial GCs')
finr=np.append(Debris[100*(timestep-1):,3],100)
finm=np.append(Debris[100*(timestep-1):,4],0)
plt.plot(finr,np.cumsum(finm),label='Deposition')

plt.yscale('log')
plt.xscale('log')
plt.xlim(10**(-3),100)
plt.ylim(10**6,10**9)
plt.legend(frameon=False,loc='upper left')
plt.xlabel(r'$r \; (\rm kpc)$')
plt.ylabel(r'$M_{\rm GC}(<r)\;(M_\odot)$')
plt.title('Mass Within Galactocentric Distance r for Halo%i'%halo)
plt.savefig('pics/Halo Running/MassRad%i.png'%halo)
plt.show()