* GC evolution for fxd/evo background potential
* Command line argument: bgsw,GCini,GCfin,Depos,(Haloevo)
* 	in input files, use '#' before lines of comments

* bgsw: background potential evolution switch, 1 for evo star+DM sersic, 0 for fxd star+DM sersic, -1 for fxd bulge+disk+DM     
* GCini: m31/mw:[1 m(msun), 2 t from now(z=3), 3 r (kpc)]
*        Hui:[1 haloID,2 logMh(z=0,3 haloID @ form,4 logMh(tform),5 logM*(tform),6 logMgas(tform),7 logMcl(tform),8 zform,9 [Fe/H],10 r(kpc)] 
* GCfin: [1 index, 2 status, 3 m(msun), 4 m_ini, 5 t_fromnow(gyr), 6 t_ini, 7 r(kpc), 8 r_ini]
*	status: 1 alive, -1 exhausted, -2 torn, -3 sunk into galaxy center
* Depos: [1 t_fromnow(gyr), 2 bin#, 3 r_inner(kpc), 4 r_outer, 5 m_depo(msun), 6 m* no evo, 7 m* wind loss, 8 lum_c(erg/s), 9 lum_eq]
*	binnub bins in a group for the same output time * (t_div-tposini) output times from past to present 
*   lum_c - eq15 of Gnedin18, lum_eq - eq14 of Gnedin18
*   delete Depos.dat everytime before each run, as new output is appended to the original
* Haloevo: [1 logMh(msun), 2 fpID, 3 subhaloID, 4 main leaf ID, 5 descID, 6 z, 7 spin x, 8 y, 9 z]
      
      PROGRAM GC_evo

      IMPLICIT real*8 (A-Z)
      integer i,j,l,N_gc,N_snap,tposini,tpos,snappos,stat,t_div,bgsw,
     &  binnub,RKbin
      real*8,parameter :: PI=3.1415926535,kpc=3.086e19
      real*8 buff                            
      character(40) GCini,Haloevo,GCfin,Depos
      character(200) buffer 

      Integer,dimension(:),allocatable::Bin_gc,active_gc           
      real*8,dimension(:),allocatable::M_gc_init,T_gc_init,R_gc_init,
     &  M_gc,T_gc,R_gc,dt_gc,mdot_td,rdot_df,Mhalo,bgTime,SpinNorm,
     &  Bin_radii     
      real*8,dimension(:,:),allocatable::GC_init,Halo_evo,M_SumBin,
     &  M_SumGC
      real*8,dimension(:,:,:),allocatable::Depo       
*     1=totalmass,2=star_no_evolution,3=star_with_evolution
*     4=luminosity by const L/M model:L=4.57*(M_depo/1e5Msun) (1e34erg/s)
*     5=luminosity by L/M model: log(L/M)=32.66+-0.06-(0.63+-0.11)logM
*       M=M_gc, so L=10**0.51*M_Deposit/M_gc**0.63 (1e34erg/s)      
      common /int/  n_gc,bgsw,binnub
      common /real8/r_min,r_max,masshalo,t_l
*==== Tunables =============================================================================
      dt_max=0.1 !upper limit of dt      
      t_div = 100   !division of t_universe. timestep starts at tposini(line153) when the 1st GC emerged
      binnub=100                 
      t_limit =1.0e-2 !lower limit of timescales, smallest tidal and migration_time is about 1e-4Gyr
*     stop GC evolution when R_gc < 0.001kpc
      R_sink = 1.0e-3
      t_universe = 13.721  !all time values in Gyr
      h=0.704         
      G=1.0
      mdot_iso_mw = 2.0/17.0      
*=================================================================================       
      call getarg(1,buffer)  
      read(buffer,*)bgsw
      call getarg(2,buffer)  
      read(buffer,*)ts_m
      call getarg(3,buffer)  
      read(buffer,*)ts_r            
      call getarg(4,GCini)!'GCini_Hui_Halo133.txt'
      call getarg(5,Depos)!'Depos_Hui_Halo133.dat'
      call getarg(6,GCfin)!'GCfin_Hui_Halo133.dat'             

      if (bgsw==1)then        
       call getarg(7,Haloevo)!'HaloBG_133.txt' 
       call getnsnap(n_snap,Haloevo)
      endif
      
      call getn(n_gc,GCini)      
      allocate(Bin_gc(N_gc),active_gc(N_gc),M_gc_init(N_gc),T_gc(N_gc),
     &  T_gc_init(N_gc),R_gc_init(N_gc),M_gc(N_gc),mdot_td(N_gc),
     &  R_gc(N_gc),dt_gc(N_gc),rdot_df(N_gc),M_SumBin(N_gc,5),
     &  Bin_radii(binnub+1),M_SumGC(binnub,5),Depo(N_gc,binnub,5))      
      if (bgsw==1)then      
       allocate(GC_init(N_gc,10),Mhalo(N_snap),SpinNorm(N_snap),
     &  bgTime(N_snap),Halo_evo(N_snap,9))  
      else
       allocate(GC_init(N_gc,3))  
      endif
      
*     read initial conditon of GCs 
      open(10,file=trim(GCini))
      j=0
      do while(.true.)
        read(10,'(A)',iostat=stat)buffer         
        if (stat/=0) then
          exit
        endif
        if (buffer(1:1)/='#') then
          j=j+1   
          read(buffer,*) GC_init(j,:)                    
        endif             
      enddo
      close(10)      

      if (bgsw==1) then       
       M_gc_init(:)=10.0**(GC_init(:,7)-5.0) ! in 10^5Msun
       R_gc_init(:)=GC_init(:,10) ! in kpc
       T_gc_init(:) = t_universe*1.0/(1.0+GC_init(:,8))**1.5
      else
       M_gc_init(:)=GC_init(:,1)/1.e5 !! in 1e5 Msun
       R_gc_init(:)=GC_init(:,3)   !! in kpc
       T_gc_init(:) = t_universe-GC_init(:,2)
      endif      
    
      M_gc(:) = M_gc_init(:)      
      R_gc(:) = R_gc_init(:)              
      T_gc(:) = T_gc_init(:)             
      t_end =  1.0*t_universe    
      dt_gc(:) = t_end           

      active_gc(:) = 1
      Depo=0.0
      if (bgsw==0)then
        prconst=41.4
      else
        prconst=100
      endif      
      
*     set up radial bins       
      R_min = 0.001
      R_max = maxval(R_gc_init) 
      Bin_radii(1) = 0.0
      do 10 l=1,binnub
      Bin_radii(l+1)=10.0**(dlog10(R_min)+(dlog10(R_max)-dlog10(R_min)) 
     &  *(real(l)-1.0)/(binnub-1))        
   10 continue
      
*     attribute bins
      do 20 i=1,N_gc
        if (R_gc(i).lt.R_min)then 
          Bin_gc(i) = 1
        else  
          Bin_gc(i) =1+floor((dlog10(R_gc(i))-dlog10(R_min))/
     &      (dlog10(R_max)-dlog10(R_min))*(binnub-1))
        endif
   20 continue
    
      if (bgsw==1)then              
*      read halo evolution
       halo_evo=0.0
       open(30,file=trim(Haloevo))
       j=0
       do while(.true.)
        read(30,'(A)',iostat=stat)buffer         
        if (stat/=0) then
          exit
        endif
        if (buffer(1:1)/='#') then
          read(buffer(1:1),*) buff
          if(buff.lt.Halo_evo(j,1))then
            exit
          else
            j=j+1   
            read(buffer,*) Halo_evo(j,:) 
          endif                   
        endif             
       enddo
       close(30)
     
       Mhalo(:)=10.0**(Halo_evo(:,1)-9.0)!in 1e9 msun
       BGTime(:)=t_universe/(1.0+Halo_evo(:,6))**1.5
       SpinNorm(:)=dsqrt(Halo_evo(:,7)**2+Halo_evo(:,8)**2+
     &  Halo_evo(:,9)**2)*kpc/h*1e3
       snappos=0             
      endif
      
      tposini=floor(minval(T_gc_init)/t_universe*t_div) 
      tpos=1+tposini      
      call cpu_time(start)      
* ========================= main routine =============================
      do while(tpos.le.t_div)
        write(*,*) tpos-tposini,'/',t_div-tposini        
        t_l = t_universe*(tpos-1)/t_div
        t_r = t_universe*tpos/t_div
        M_SumBin = sum(Depo,dim=2)
        M_SumGC = sum(Depo,dim=1)
        
        if(bgsw==1)then
          do while(snappos/=N_snap)
            if(bgtime(snappos+1).le.t_l) then
              snappos=snappos+1 
            else
              exit
            endif                           
          enddo        
        
          if (snappos.eq.0) then
            masshalo=mhalo(1)
          else if (snappos.eq.n_snap)then
              masshalo=mhalo(n_snap)
            else
              masshalo=mhalo(snappos)+(mhalo(snappos+1)-mhalo(snappos))*
     &         (t_l-bgtime(snappos))/(bgtime(snappos+1)-bgtime(snappos))
          endif
        endif
        
*------ update all dt(i), and find gc with smallest t+dt --------------- 
        do 30 i=1,N_gc          
          if((T_gc(i).lt.t_r).and.(active_gc(i).eq.1).and.(M_gc_init(i)
     &      .gt.1e-2))then                 
*           find rho_all, Vc
            select case(bgsw)
              case(1)
              rho_bg=rho_bg_sw1(r_gc(i),spinnorm(snappos))
              case(0)
              rho_bg=rho_bg_sw0(r_gc(i))
              case(-1)
              rho_bg=rho_bg_swm1(r_gc(i))
            end select
            M_encloseR=sum(M_SumGC(1:(Bin_gc(i)-1),1),dim=1)
            rho_add = M_encloseR/(4./3*PI*R_gc(i)**3)/1.e4        
            rho_bg = rho_bg+rho_add
*           average density at half radius of GCs
            rho_h = 1.0e3*M_gc(i)**2
            if (M_gc(i) .lt. 1.0) then 
              rho_h = 1.0e3
            elseif (M_gc(i) .gt. 10.0) then 
              rho_h = 1.0e5
            endif
        
*           check direct tidal tear
            if (rho_h .lt. rho_bg) then
              active_gc(i) = -2
              dt_gc(i) = t_end
              
              Depo(i,Bin_gc(i),1)=Depo(i,Bin_gc(i),1)+M_gc(i)
              Depo(i,Bin_gc(i),2)=Depo(i,Bin_gc(i),2)+M_gc(i)
              Depo(i,Bin_gc(i),3)=Depo(i,Bin_gc(i),3)+M_gc(i)
              Depo(i,Bin_gc(i),4)=Depo(i,Bin_gc(i),4)+4.57*M_gc(i)
              Depo(i,Bin_gc(i),5)=Depo(i,Bin_gc(i),5)+(10.0**0.51)
     &          *M_gc(i)**0.37

              M_gc(i)=0.
            else
*             calculate Mdot
              P_r = prconst*R_gc(i)/Vc(M_encloseR,R_gc(i),rho_bg)              
              Mdot_td(i)=2.0**(2./3.)*0.1*M_gc(i)**(1./3.)/P_r
              if(bgsw==0)then
                mdot_td(i)=amax1(mdot_iso_mw,mdot_td(i))
              endif  
              dtm=ts_m*M_gc(i)/Mdot_td(i)
              if ((T_gc(i)+dtm).gt. t_r) then
                dtm = t_r-T_gc(i)
              elseif (dtm .lt. ts_m*t_limit) then
                dtm = ts_m*t_limit
              endif  
                          
*             calculate Rdot using Runge-Kutta 4th                          
              rdot1=M_gc(i)/0.45/r_gc(i)/Vc(M_encloseR,R_gc(i),rho_bg)     
              hrk=ts_r*r_gc(i)/rdot1
              if ((T_gc(i)+hrk).gt. t_r) then
                hrk = t_r-T_gc(i)
              elseif (hrk .lt. ts_r*t_limit) then
                hrk = ts_r*t_limit
              endif  
              dt_gc(i)=min(min(dtm,hrk),dt_max)
              hrk=dt_gc(i)                           
              k1 = hrk * rdot1
      
              r2 = r_gc(i) - 0.5 * k1
              if(r2.gt.0.) then 
                call rhobg_bin(rho_bg,RKbin,R2,spinnorm(snappos))
                M_encloseR=sum(M_SumGC(1:(rkBin-1),1),dim=1)
                rdot2=M_gc(i)/0.45/r2/Vc(M_encloseR,r2,rho_bg)      
                k2 = hrk * rdot2
              
                r3 = r_gc(i) - 0.5 * k2
                if(r3.gt.0.) then  
                  call rhobg_bin(rho_bg,RKbin,R3,spinnorm(snappos))
                  M_encloseR=sum(M_SumGC(1:(rkBin-1),1),dim=1)
                  rdot3=M_gc(i)/0.45/r3/Vc(M_encloseR,r3,rho_bg)      
                  k3 = hrk * rdot3
       
                  r4 = r_gc(i) - k3
                  if(r4.gt.0.) then              
                    call rhobg_bin(rho_bg,RKbin,R4,spinnorm(snappos))
                    M_encloseR=sum(M_SumGC(1:(rkBin-1),1),dim=1)
                    rdot4=M_gc(i)/0.45/r4/Vc(M_encloseR,r4,rho_bg)      
                    k4 = hrk * rdot4 
                  else
                    k4=1e10      
                  endif
                else 
                  k3=1e10
                  k4=1e10
                endif
              else 
                k2=1e10
                k3=1e10
                k4=1e10
              endif
              
              rdot_df(i)=(k1+2*k2+2*k3+k4)/6.0/hrk 
                      
            endif !! 214, if GC is torn apart            
          endif !! 189, if T_gc<t_r, active, and with ample mass
  30    continue !! 188, all GCs checked

        i = minloc(T_gc(:) +dt_gc(:),dim=1)
        !! the GC to be evolved first

*------ keep evolving the right GC(i) till all gc t+dt=t_r -----------------
*       evolve,check status,update dt,find the next gc to evolve 
        do while (T_gc(i)+dt_gc(i) .lt. t_r)    
          M_SumBin = sum(Depo,dim=2)
          M_SumGC = sum(Depo,dim=1)                         
*         stellar wind for deposited stars from GC(i)     
          do 40 l=1,binnub
            dM_star=M_gc_init(i)*Depo(i,l,3)/(M_gc(i)+M_SumBin(i,3))*
     &        (swf(T_gc(i)+dt_gc(i)-T_gc_init(i))
     &        -swf(T_gc(i)-T_gc_init(i)))     
              Depo(i,l,3)=max(0.0,Depo(i,l,3)-dM_star)              
   40     continue
   
*         stellar wind for GC(i)
          dM_gc_sw=M_gc_init(i)*M_gc(i)/(M_gc(i)+M_SumBin(i,3))*
     &      (swf(T_gc(i)+dt_gc(i)-T_gc_init(i))-
     &      swf(T_gc(i)-T_gc_init(i)))       
          dM_gc_sw=min(M_gc(i),dM_gc_sw)                                    
          M_gc(i) = M_gc(i)-dM_gc_sw
*         tidal strip                           
          dM_gc=min(M_gc(i),dt_gc(i)*Mdot_td(i))
          M_gc(i) = M_gc(i)-dM_gc 
                     
          Depo(i,Bin_gc(i),1)=Depo(i,Bin_gc(i),1)+dM_gc+dM_gc_sw
          Depo(i,Bin_gc(i),2)=Depo(i,Bin_gc(i),2)+dM_gc
          Depo(i,Bin_gc(i),3)=Depo(i,Bin_gc(i),3)+dM_gc
          Depo(i,Bin_gc(i),4)=Depo(i,Bin_gc(i),4)+4.57*dM_gc
          Depo(i,Bin_gc(i),5)=Depo(i,Bin_gc(i),5)+(10.0**0.51)*dM_gc/
     &      (M_gc(i)**0.63)

*         dynamical friction 
          dR = dt_gc(i)*Rdot_df(i)
          R_gc(i) = max(0.0,R_gc(i)-dR)
           
          if (R_gc(i).lt.R_min)then 
            Bin_gc(i) = 1
          else  
            Bin_gc(i) =1+floor((dlog10(R_gc(i))-dlog10(R_min))/
     &        (dlog10(R_max)-dlog10(R_min))*(binnub-1))
          endif                        
            
          T_gc(i) = T_gc(i)+dt_gc(i)          
* --------------------------------------------------

*         check status
          select case(bgsw)
            case(1)
            rho_bg=rho_bg_sw1(r_gc(i),spinnorm(snappos))
            case(0)
            rho_bg=rho_bg_sw0(r_gc(i))
            case(-1)
            rho_bg=rho_bg_swm1(r_gc(i))
          end select
          M_encloseR=sum(M_SumGC(1:(Bin_gc(i)-1),1),dim=1)
          rho_add = M_encloseR/(4./3*PI*R_gc(i)**3)/1.e4
          rho_bg=rho_bg+rho_add

          rho_h = 1.0e3*M_gc(i)**2
          if (M_gc(i) .lt. 1.0) then
            rho_h = 1.0e3
          elseif (M_gc(i) .gt. 10.0) then
            rho_h = 1.0e5
          endif
            
          if((M_gc(i).le.0.0).or.(R_gc(i).le.R_sink).or.(rho_h.lt.rho_bg
     &      )) then  
            dt_gc(i) = t_end
            if (M_gc(i).le.0.0)then  
              active_gc(i) = -1
            else
              Depo(i,Bin_gc(i),1)=Depo(i,Bin_gc(i),1)+M_gc(i)
              Depo(i,Bin_gc(i),2)=Depo(i,Bin_gc(i),2)+M_gc(i)
              Depo(i,Bin_gc(i),3)=Depo(i,Bin_gc(i),3)+M_gc(i)
              Depo(i,Bin_gc(i),4)=Depo(i,Bin_gc(i),4)+4.57*M_gc(i)
              Depo(i,Bin_gc(i),5)=Depo(i,Bin_gc(i),5)+(10.0**0.51)
     &          *M_gc(i)**0.37
              m_gc(i)=0.0
              if (R_gc(i).le.R_sink) then
                active_gc(i) = -3
              else
                active_gc(i) = -2
              endif
            endif

          else !still active        
*           update mrdot,dt   
            P_r = prconst*R_gc(i)/Vc(M_encloseR,R_gc(i),rho_bg)
            Mdot_td(i)=2.0**(2./3.)*0.1*M_gc(i)**(1./3.)/P_r
            if(bgsw==0)then
              mdot_td(i)=amax1(mdot_iso_mw,mdot_td(i))
            endif             
            dtm=ts_m*m_gc(i)/mdot_td(i)
            if ((T_gc(i)+dtm).gt. t_r) then
              dtm = t_r-T_gc(i)
            elseif (dtm .lt. ts_m*t_limit) then
              dtm = ts_m*t_limit
            endif              
*           calculate Rdot using Runge-Kutta 4th                          
            rdot1=M_gc(i)/0.45/r_gc(i)/Vc(M_encloseR,R_gc(i),rho_bg)     
            hrk=ts_r*r_gc(i)/rdot1 
            if ((T_gc(i)+hrk).gt. t_r) then
              hrk = t_r-T_gc(i)
            elseif (hrk .lt. ts_r*t_limit) then
              hrk = ts_r*t_limit
            endif         
            dt_gc(i)=min(min(dtm,hrk),dt_max)
            hrk=dt_gc(i)
            
            k1 = hrk * rdot1
      
            r2 = r_gc(i) - 0.5 * k1
            if(r2.gt.0.) then 
              call rhobg_bin(rho_bg,RKbin,R2,spinnorm(snappos))
              M_encloseR=sum(M_SumGC(1:(rkBin-1),1),dim=1)
              rdot2=M_gc(i)/0.45/r2/Vc(M_encloseR,r2,rho_bg)      
              k2 = hrk * rdot2
              
              r3 = r_gc(i) - 0.5 * k2
              if(r3.gt.0.) then  
                call rhobg_bin(rho_bg,RKbin,R3,spinnorm(snappos))
                M_encloseR=sum(M_SumGC(1:(rkBin-1),1),dim=1)
                rdot3=M_gc(i)/0.45/r3/Vc(M_encloseR,r3,rho_bg)      
                k3 = hrk * rdot3
       
                r4 = r_gc(i) - k3
                if(r4.gt.0.) then              
                  call rhobg_bin(rho_bg,RKbin,R4,spinnorm(snappos))
                  M_encloseR=sum(M_SumGC(1:(rkBin-1),1),dim=1)
                  rdot4=M_gc(i)/0.45/r4/Vc(M_encloseR,r4,rho_bg)      
                  k4 = hrk * rdot4 
                else
                  k4=1e10     
                endif
              else 
                k3=1e10
                k4=1e10
              endif
            else 
              k2=1e10
              k3=1e10
              k4=1e10
            endif        
        
            rdot_df(i)=(k1+2*k2+2*k3+k4)/6.0/hrk
                                                  
          endif                                
       
          i = minloc(T_gc(:)+dt_gc(:),dim=1)
        end do !250, while t+dt<t_r

* ----- go to output stage at t_r ----------------------
        M_SumBin = sum(Depo,dim=2)
        M_SumGC = sum(Depo,dim=1)
        do 50 i=1,N_gc
          if((T_gc(i).lt.t_r).and.(active_gc(i).eq.1).and.(M_gc_init(i)
     &      .gt.1e-2))then
            do 60 l=1,binnub                               
              dM_star=M_gc_init(i)*Depo(i,l,3)/(M_gc(i)+M_SumBin(i,3))
     &          *(swf(T_gc(i)+dt_gc(i)-T_gc_init(i))-
     &          swf(T_gc(i)-T_gc_init(i)))       
              Depo(i,l,3)=Depo(i,l,3)-dM_star
  60        continue
          
            dM_gc_sw=M_gc_init(i)*M_gc(i)/(M_gc(i)+M_SumBin(i,3))*
     &        (swf(T_gc(i)+dt_gc(i)-T_gc_init(i))-
     &        swf(T_gc(i)-T_gc_init(i)))      
            dM_gc_sw=min(M_gc(i),dM_gc_sw)    
            M_gc(i)=M_gc(i)-dM_gc_sw

            dM_gc=dt_gc(i)*Mdot_td(i)
            dM_gc=min(dM_gc,M_gc(i))
            M_gc(i)=M_gc(i)-dM_gc

            Depo(i,Bin_gc(i),1)=Depo(i,Bin_gc(i),1)+dM_gc+dM_gc_sw                                          
            Depo(i,Bin_gc(i),2)=Depo(i,Bin_gc(i),2)+dM_gc
            Depo(i,Bin_gc(i),3)=Depo(i,Bin_gc(i),3)+dM_gc
            Depo(i,Bin_gc(i),4)=Depo(i,Bin_gc(i),4)+4.57*dM_gc
            Depo(i,Bin_gc(i),5)=Depo(i,Bin_gc(i),5)+(10.0**0.51)*
     &        dM_gc/(M_gc(i)**0.63)

            dR = dt_gc(i)*Rdot_df(i)  
            R_gc(i)=max(0.0,R_gc(i)-dR)            
            if (R_gc(i).lt.R_min)then 
              Bin_gc(i) = 1
            else  
              Bin_gc(i) =1+floor((dlog10(R_gc(i))-dlog10(R_min))/
     &          (dlog10(R_max)-dlog10(R_min))*(binnub-1))
            endif            
            T_gc(i) = t_r 
            
*           check status
            select case(bgsw)
              case(1)
              rho_bg=rho_bg_sw1(r_gc(i),spinnorm(snappos))
              case(0)
              rho_bg=rho_bg_sw0(r_gc(i))
              case(-1)
              rho_bg=rho_bg_swm1(r_gc(i))
            end select
            M_encloseR=sum(M_SumGC(1:(Bin_gc(i)-1),1),dim=1)
            rho_add = M_encloseR/(4./3*PI*R_gc(i)**3)/1.e4
            rho_bg=rho_bg+rho_add   
                   
            rho_h = 1.0e3*M_gc(i)**2
            if (M_gc(i) .lt. 1.0) then
              rho_h = 1.0e3
            elseif (M_gc(i) .gt. 10.0) then
              rho_h = 1.0e5
            endif
            
            if ((M_gc(i).le.0.0).or.(R_gc(i).le.R_sink).or.(rho_h.lt.
     &        rho_bg)) then  
              dt_gc(i) = t_end
              if (M_gc(i).le.0.0)then  
                active_gc(i) = -1
              else
                Depo(i,Bin_gc(i),1)=Depo(i,Bin_gc(i),1)+M_gc(i)
                Depo(i,Bin_gc(i),2)=Depo(i,Bin_gc(i),2)+M_gc(i)
                Depo(i,Bin_gc(i),3)=Depo(i,Bin_gc(i),3)+M_gc(i)
                Depo(i,Bin_gc(i),4)=Depo(i,Bin_gc(i),4)+4.57*M_gc(i)            
                Depo(i,Bin_gc(i),5)=Depo(i,Bin_gc(i),5)+(10.0**0.51)*
     &            M_gc(i)**0.37
                m_gc(i)=0.0
                if (R_gc(i).le.R_sink) then
                  active_gc(i) = -3
                else
                  active_gc(i) = -2
                endif
              endif
            endif ! if not active
                        
          endif  ! 351, this GC is evolved to t_r
  50    continue ! 350, all qualified GCs evolved to t_r

*       output depos
        M_SumGC = sum(Depo,dim=1) ! sum up mass from all GCs
        open(70,position="append",file=trim(Depos))
          do 80 l=1,100
            WRITE(70,*) t_end-t_r,l,Bin_radii(l),Bin_radii(l+1),
     &      10**5*M_SumGC(l,1),10**5*M_SumGC(l,2),10**5*M_SumGC(l,3),
     &      1.0e34*M_SumGC(l,4),1.0e34*M_SumGC(l,5)
  80      continue
        close(70)        
        tpos=tpos+1
        
        call cpu_time(record)
        write(*,*)'run time',record-start,'seconds'        
      enddo ! 161, reach last t_r
      
* --- output final GCs ----------------------------------------------- 
      open(90,file=trim(GCfin))
        do 100 i=1,N_gc
          WRITE(90,*) i,active_gc(i),10**5*M_gc(i),10**5*M_gc_init(i),
     &    t_end-T_gc(i),t_end-T_gc_init(i),R_gc(i),R_gc_init(i)          
  100   continue
      close(90)

      deallocate(Bin_gc,active_gc,M_gc_init,T_gc,T_gc_init,R_gc_init,
     &  M_gc,mdot_td,R_gc,dt_gc,rdot_df,GC_init,Depo,M_SumBin,bin_radii,
     &  m_sumgc)
      if(bgsw==1)then
        deallocate(bgTime,Halo_evo,SpinNorm)
      endif
       
      END


*==== Functions =====================================================
*---- stellar wind loss fraction ------------------------------------
      real*8 function swf(t)
      real*8 t ! in gyr
      swf=amax1(0.,-(dlog10(t)+9)**2/100. + 0.288*(dlog10(t)+9)-1.42)
      end
*---- virial radius --------------------------------------------------
      real*8 function rvir()
      implicit real*8 (a-z)
      common /real8/ a,b,m,t
      t_universe=13.721      
      PI=3.1415926535      
      h=0.704      
      Om0=0.273
      Od0=1-Om0
      H0=2.33e-18
      q0=-0.55
      t0=4.42e17
      zc=0.6818
      alpha=H0*t0/(1-H0*t0*(1+q0))
      beta=1-H0*t0*(1+q0)
      lamda=log(0.5/Od0)/(1-(1-log(1+zc)/alpha)**(1/beta))
      mu=beta*log(2*Od0)/log(1+1/alpha*log(1/(1+zc)))        
      usez=min(5.816,(t_universe/t)**(2./3.)-1)!Omem functions saturate beyond 5.816              
      Omem1=1-Od0*(1-log(1+usez)/alpha)**(-mu/beta) 
   	  Omem2=1-od0*exp(lamda*(1.0-(1.0-log(1+usez)/alpha)**(1/beta)))
      Omem =(Omem1+Omem2)/2
      delvir=(18*pi**2+82*(Omem-1)-39*(Omem-1)**2)/Omem    
      Rvir=163/h*(m/1e3*h)**(1./3)/(Om0*delvir/200)**(1./3)/(1+usez)
      end 
*---- mstar at t_l with masshalo ---------------------------------------------------
      real*8 function mstar()
      implicit real*8 (a-z)   
      common /real8/ rmin,ramx,m,t
      t_universe=13.721
      zero=0.      
      a=1./(t_universe/t)**(2./3.)
      nu=exp(-4*a**2)
      epsilon=10**(-1.777-0.006*(a-1)*nu-0.119*(a-1))
      m1=10**(11.514-nu*(1.793*(a-1)+0.251*((t_universe/t)**(2./3.)-1)))       
      mstar=epsilon*m1*10**(fx(log10(m*1e9/m1),(t_universe/t)**(2./3.)-
     &  1)-fx(zero,(t_universe/t)**(2./3.)-1))/1e9        
      
      end
*---- function fx in mstar() -----------------------------------
      function fx(x,z)
       implicit real*8 (a-z)
       a=1./(1+z)
       nu=exp(-4*a**2)
       alpha=-1.412+0.731*(a-1)*nu
       delta=3.508+nu*(2.608*(a-1)-0.043*z)
       gamma=0.316+nu*(1.319*(a-1)+0.279*z)
       fx=-log10(10**(alpha*x)+1)+delta*(log10(1+exp(x)))**gamma/
     & (1+exp(.1**x))       
       return
      end       
*---- rho_bg_sw1 --------------------------------------------------------------------
      real*8 function rho_bg_sw1(r,spinnorm)
      implicit real*8 (a-z)
      common /real8/ a,b,mhalo,tl     
      G=1.      
      h=0.704
      kpc=3.086e19 
      pi=3.1415926535     
      sersic_n = 2.2 
      sersic_p = 1.0-0.6097/sersic_n+0.05563/sersic_n**2
      sersic_b = 2.*sersic_n-1./3+0.009876/ sersic_n                

      c=9.354/(mhalo*h/1e3)**0.094
      DM_rs=Rvir()/c                 
      dphidr_DM=G*mhalo*(dlog(1.0+r/DM_rs)/r**2-1.0/(DM_rs+r)/r)     

 	  spinprm=spinnorm/sqrt(2*6.67*Rvir()*kpc*mhalo*2e28)          
      sersic_Re=spinprm*Rvir()/sqrt(2.0)               
      sersic_z = sersic_b*(R/sersic_Re)**(1./sersic_n)
      M_ser_r=Mstar()*gammp(sersic_n*(3.0-sersic_p),sersic_z)  
      Vcbg_r=dsqrt(r*dphidr_DM+G*M_ser_r/r)
      rho_bg_sw1 = Vcbg_r**2/(4./3*PI*G*r**2)
       
      end      
*---- rho_bg_sw0 -----------------------------------------------
      real*8 function rho_bg_sw0(r)
      implicit real*8 (a-z)  
      G=1.  
      pi=3.1415926535    
      Mstarmw=50.0 
      sersic_Re = 4.0  
      M_DM=1.0e3  
      DM_rs=20.0
      sersic_n = 2.2 
      sersic_p = 1.0-0.6097/sersic_n+0.05563/sersic_n**2
      sersic_b = 2.*sersic_n-1./3+0.009876/ sersic_n
      
      dphidr_DM=G*M_DM*(dlog(1.0+r/DM_rs)/r**2-1./(DM_rs+r)/r)    
      sersic_z = sersic_b*(r/sersic_Re)**(1./sersic_n)
      M_sersic_r=Mstarmw*gammp(sersic_n*(3.0-sersic_p),sersic_z) !! in 1e9 Msun
      Vcbg_r=dsqrt(r*dphidr_DM+G*M_sersic_r/r)
      rho_bg_sw0 = Vcbg_r**2/(4./3*PI*G*R**2)            
      end
*---- rho_bg_swm1 ----------------------------------------------
      real*8 function rho_bg_swm1(r)
      implicit real*8 (a-z)  
      G=1.  
      pi=3.1415926535          
      M_bulge=19.0
      bulge_a = 1.0
      M_disk=80.0
      disk_b=5.0
      disk_c=1.0
      M_DM=2.0e3
      DM_rs=35.0 
      Z_gc=0.
      rxy=r               

      dphidr_bul = G*M_bulge/(r+bulge_a)**2
      dphidr_disk = G*M_disk*(rxy**2/r+(disk_b+dsqrt(disk_c**2+Z_gc**2))
     &  *(Z_gc**2/r)/dsqrt(disk_c**2+Z_gc**2))/(Rxy**2+(disk_b+
     &  dsqrt(disk_c**2+Z_gc**2))**2)**1.5    
      dphidr_DM = G*M_DM*(dlog(1.0+r/DM_rs)/r**2-1.0/(DM_rs+r)/r)
      Vcbg_r= dsqrt(r*(dphidr_bul+dphidr_disk+dphidr_DM)) 
      rho_bg_swm1 = Vcbg_r**2/(4./3*PI*G*R**2)                       
      end  
*---- get Vc(km/s)-------------------------------------------------
      real*8 function Vc(m_r,r,rho_bg)
      real*8 r,rho_add,m_r,pi,rho_bg,G
      pi=3.1415926
      G=1.0
      
      rho_add = M_R/(4./3*PI*R**3)/1.e4   
*     sqrt(G*1e9Msun/1kpc)=65.7677131526         
      Vc=dsqrt((rho_bg+rho_add)*(4./3*PI*G*R**2))*65.7677131526
      end       
*-----------------------------------------------------------------------
      subroutine getN(n,filename)
*      get the 1st dimension for an ndarray in the filename, skipping lines of comment starting with '#'      
       integer n,stat
       character(40) filename,buffer
      
       open(10,file=trim(filename))            
       n=0
       stat=0
       do while(.true.)
          read(10,'(A)',iostat=stat)buffer         
          if (stat/=0) then
            exit
          endif
          if (buffer(1:1)/='#') then
           N=N+1   
*           write(*,*) buffer,n,stat           
          endif             
       enddo
       close(10)
      end  
*--------------------------------------
      subroutine getnsnap(n,filename)    
       integer n,stat
       real*8 memory,buff
       character(40) filename,buffer
      
       open(10,file=trim(filename))            
       n=0
       stat=0
       memory=0.
       do while(.true.)
          read(10,'(A)',iostat=stat)buffer          
          if (stat/=0) then
            exit
          endif
          if (buffer(1:1)/='#') then  
            read(buffer(1:1),*) buff   
            if(buff.lt.memory)then
              exit
            else
              N=N+1     
              memory=buff             
            endif    
          endif             
       enddo
       close(10)
      end      
*-----------------------------------------------------------------
      subroutine rhobg_bin(rho,bin,r,spinnorm)
       real*8 r,r_min,r_max,rho,spinnorm,b,c
       real*8 rho_bg_sw0,rho_bg_swm1,rho_bg_sw1
       INTEGER bgsw,binnub,bin
       common /int/ a,bgsw,binnub
       common /real8/ r_min,r_max,b,c
       
       select case(bgsw)
        case(1)
        rho=rho_bg_sw1(r,spinnorm)
        case(0)
        rho=rho_bg_sw0(r)
        case(-1)
        rho=rho_bg_swm1(r)
       end select  
                          
       if (r.lt.R_min)then 
         Bin = 1
       else  
         Bin =1+floor((dlog10(R)-dlog10(R_min))/(dlog10(R_max)
     &     -dlog10(R_min))*(binnub-1))
       endif
      END      
*===================================================================
** function of incompleted gamma, for sersic model
** source code from numerical recipes in fortran77
*=====================================================================
        real*8 FUNCTION gammp(a,x)
        real*8 a,x
C USESgcf,gser
C Returns the incomplete gamma function P(a,x).
        real*8 gammcf,gamser,gln
***        write(6,*) a,x
        if(x.lt.0. .or.a.le.0.) then
                print *,'bad arguments in gammp'
                gammp = 0.
                return
        endif
        if(x.lt.a+1.) then
C Use the series representation.
                call gser(gamser,a,x,gln)
                gammp=gamser
        else
C Use the continued fraction representation
                call gcf(gammcf,a,x,gln)
                gammp=1.-gammcf
        endif
        return
        END

*=================================================
        real*8 FUNCTION gammln(xx)
        real*8 xx
        INTEGER j
        DOUBLE PRECISION ser,stp,tmp,x,y,cof(6)
        SAVE cof,stp
        DATA cof,stp/76.18009172947146d0,-86.50532032941677d0,
     &  24.01409824083091d0,-1.231739572450155d0,.1208650973866179d-2,
     &  -.5395239384953d-5,2.5066282746310005d0/
        x=xx
        y=x
        tmp=x+5.5d0
        tmp=(x+0.5d0)*log(tmp)-tmp
        ser=1.000000000190015d0
        do 11 j=1,6
                y=y+1.d0
                ser=ser+cof(j)/y
   11   continue
        gammln=tmp+log(stp*ser/x)
        return
        END

*=================================================
        SUBROUTINE gser(gamser,a,x,gln)
        INTEGER ITMAX
        real*8 a,gamser,gln,x,EPS
        PARAMETER (ITMAX=100,EPS=3.e-7)
C USESgammln
C Returns the incomplete gamma function P(a,x) evaluated by its series
C representation as
C gamser. Also returns ln(a) as gln. 
        INTEGER n
        real*8 ap,del,sum,gammln
        gln=gammln(a)
        if(x.le.0.)then
                if(x.lt.0.) print *,'x < 0 in gser'
                gamser=0.
                return
        endif
        ap=a
        sum=1./a
        del=sum
        do 11 n=1,ITMAX
                ap=ap+1.
                del=del*x/ap
                sum=sum+del
                if(abs(del).lt.abs(sum)*EPS) goto 20
   11  continue
        print *, 'a too large, ITMAX too small in gser'
   20    gamser=sum*exp(-x+a*log(x)-gln)
        return
        END

*=================================================
        SUBROUTINE gcf(gammcf,a,x,gln)
        INTEGER ITMAX
        real*8 a,gammcf,gln,x,EPS,FPMIN
        PARAMETER (ITMAX=100,EPS=3.e-7,FPMIN=1.e-30)
C USESgammln
C Returns the incomplete gamma function Q(a, x) evaluated by its 
C continued fraction representation as gammcf. Also returns ln (a) as gln.
C Parameters: ITMAX is the maximum allowed number of iterations; EPS
C is the relative accuracy; FPMIN is a number near the smallest
C representable floating-point number.
        INTEGER i
        real*8 an,b,c,d,del,h,gammln
        gln=gammln(a)
        b=x+1.-a
        c=1./FPMIN
        d=1./b
        h=d
        do 11 i=1,ITMAX
                an=-i*(i-a)
                b=b+2.
                d=an*d+b
                if(abs(d).lt.FPMIN) d=FPMIN
                c=b+an/c
                if(abs(c).lt.FPMIN) c=FPMIN
                d=1./d
                del=d*c
                h=h*del
                if(abs(del-1.).lt.EPS) goto 20
   11   continue
        print *,'a too large, ITMAX too small in gcf'
   20  gammcf=exp(-x+a*log(x)-gln)*h
C Put factors in front.
        return
        END          