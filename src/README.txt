1.GC evolution: GChui_bgevo_GY.f 
   Initial GCs and halos should be stored in separate files for different halos 
   modify 'Tunables'
2.GC properties check after evolution: Evo_Check.py
   modify 'Input'
3.MSP sampling: MSP_Debris_TotalSampling.py
   to be intrisically used by 4 below. Modify 'Input' session.
4.MSP luminosity evolution and final lum plot:  MSP_LumEvo.py
   modify the plot title at the end, and run directly
