import numpy as np
import matplotlib.pyplot as plt

gyr=10**9*365*24*3600
def tau(B,P):
    return 1.8868*10**17*(2*10**8/B)**2*(P/2)**2/gyr

i=0
TAUBP=[]
TAU=[]

while i<45000:
    B=10**np.random.normal(8.47,0.33)
    P=10**np.random.normal(0.476,0.234)
    Tau=np.random.normal(3,1)
    TAUBP.append(tau(B,P))
    TAU.append(Tau)
    i+=1

while i<100000:
    B=10**np.random.normal(8.47,0.33)
    P=10**np.random.normal(0.476,0.234)
    TAUBP.append(tau(B,P))
    i+=1

binset=[0]
j=0
while j<8000:
    binset.append(j+0.25)
    j+=0.25
    
plt.hist(TAUBP,binset,histtype='step',label='Model LON')
plt.hist(TAU,binset,histtype='step',label='Model GAU')
plt.legend(loc='upper right')
plt.xlim(0,10)
plt.show()