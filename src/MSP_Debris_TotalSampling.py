import numpy as np
# all masses in msun, lengths in kpc, um in erg/s

pc=3.09*10**16
kpc=10**3*pc
Lgamma_min=10**31 
Lgamma_max=10**36
# Input ######################################################################
Debris=np.loadtxt('Fortran/Depos_Hui_Halo133.dat')
#debris: [t_fromnow(gyr), bin#, r_in(kpc), r_out, m_depo(msun), m* no evo, m* wind loss, lum_c(erg/s), lum_eq]
timestep=int(len(Debris)/100) # = t_div - tposini  
##############################################################################

def LgammaEQ(Mgc):   
    return Mgc**0.37*10**32.66 # small scatter in the parameters omitted 
def LgammaC(Mgc):
    return Mgc*4.57e29


# deduct the previous deposition from the same radial bins
i=100*(timestep-1) #for time steps
j=0 #for radial bins, 100

while i>99:
    while j<100:
        Debris[i+j,7]-=Debris[i+j-100,7]        
        Debris[i+j,8]-=Debris[i+j-100,8]        
        j+=1
    i-=100
    j=0
    
# sample MSP lums from total debris lum
LumAve=8.69e34
NumAveC=int(1.2*sum(Debris[:,7])/LumAve)
NumAveEQ=int(1.2*sum(Debris[:,8])/LumAve)

MSPLumC=10**(31+5*np.random.random(size=NumAveC))
MSPLumEQ=10**(31+5*np.random.random(size=NumAveEQ))
MSPLumC=MSPLumC[np.cumsum(MSPLumC)<sum(Debris[:,7])]
MSPLumEQ=MSPLumEQ[np.cumsum(MSPLumEQ)<sum(Debris[:,8])]

#print('total Lum-C = ',sum(Debris[:,7]))
#print('desgn Lum-C = ',sum(MSPLumC))
#print('total Lum-EQ = ',sum(Debris[:,8]))
#print('desgn Lum-EQ = ',sum(MSPLumEQ))

# distribute MSP lums to deposition points
# first assign # of MSP lums to each debris
while 1:
    LumNumbC=np.random.poisson(Debris[:,7]/LumAve,100*timestep)
    if sum(LumNumbC)==len(MSPLumC):
        break
#print('total MSP-C number is ', len(MSPLumC))
#print('desgn MSP-C number is ', sum(LumNumbC))

while 1:
    LumNumbEQ=np.random.poisson(Debris[:,8]/LumAve,100*timestep)
    if sum(LumNumbEQ)==len(MSPLumEQ):
        break

#print('total MSP-EQ number is ', len(MSPLumEQ))
#print('desgn MSP-EQ number is ', sum(LumNumbEQ))

#then compile the t list and r list
tlistC=[]
rlistC=[]
for i in range(len(LumNumbC)):
    for j in range(LumNumbC[i]):
        tlistC.append(Debris[i,0])
        rlistC.append(Debris[i,2]+(Debris[i,3]-Debris[i,2])*np.random.random())
ListMSPC=np.array([MSPLumC,#[0:-1]
                   tlistC,
                   rlistC
                  ]).T

tlistL=[]
rlistL=[]
for i in range(len(LumNumbEQ)):
    for j in range(LumNumbEQ[i]):
        tlistL.append(Debris[i,0])
        rlistL.append(Debris[i,2]+(Debris[i,3]-Debris[i,2])*np.random.random())
ListMSPEQ=np.array([MSPLumEQ,#[0:-1]
                   tlistL,
                   rlistL
                  ]).T
# sort in radial order
ListMSPC=ListMSPC[ListMSPC[:,2].argsort()]
ListMSPEQ=ListMSPEQ[ListMSPEQ[:,2].argsort()]