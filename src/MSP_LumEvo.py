import MSP_Debris_TotalSampling as spl
import matplotlib.pyplot as plt
import numpy as np
plt.style.use('classic')
# all masses in msun, lengths in kpc, lum in erg/s, time in gyr

gyr=10**9*365*24*3600

MSPLONC=np.copy(spl.ListMSPC)
MSPGAUC=np.copy(spl.ListMSPC)
MSPLONEQ=np.copy(spl.ListMSPEQ)
MSPGAUEQ=np.copy(spl.ListMSPEQ)

def tau(B,P):
    return 1.8868*10**17*(2*10**8/B)**2*(P/2)**2/gyr 
# Model LON #
  # B: log10-norm mean 8.47, sd 0.33
  # P: log-norm mean 3 ms, dev 0.234 
# Model GAU: tau norm mean 3 gyr, dev 1

# modify the first entry of each MSP to its final luminosity
MSPBPSampleC=np.array([10**np.random.normal(8.47,0,MSPLONC.shape[0]),          
                        np.random.lognormal(0.476,0,MSPLONC.shape[0])
                       ]).T  
MSPBPSampleEQ=np.array([10**np.random.normal(8.47,0,MSPLONEQ.shape[0]),          
                        np.random.lognormal(0.476,0,MSPLONEQ.shape[0])
                       ]).T                          
MSPTauSampleC=np.array(abs(np.random.normal(3,1,MSPGAUC.shape[0])))
MSPTauSampleEQ=np.array(abs(np.random.normal(3,1,MSPGAUEQ.shape[0])))

def SpindownLON(t,B,P):
    return (1+np.sqrt(t/tau(B,P)))**2
def SpindownGAU(t,Tau):
    return (1+np.sqrt(t/Tau))**2

MSPLONC[:,0]/= SpindownLON(MSPLONC[:,1],MSPBPSampleC[:,0],MSPBPSampleC[:,1])
MSPLONEQ[:,0]/= SpindownLON(MSPLONEQ[:,1],MSPBPSampleEQ[:,0],MSPBPSampleEQ[:,1])
MSPGAUC[:,0]/= SpindownGAU(MSPGAUC[:,1],MSPTauSampleC)
MSPGAUEQ[:,0]/= SpindownGAU(MSPGAUEQ[:,1],MSPTauSampleEQ)

# Finally sum the cumulative final lum
MSPLCumLONC=np.array([np.copy(MSPLONC[:,2]),
                       np.cumsum(MSPLONC[:,0])
                      ]).T
MSPLCumLONEQ=np.array([np.copy(MSPLONEQ[:,2]),
                       np.cumsum(MSPLONEQ[:,0])
                      ]).T
MSPLCumGAUC=np.array([np.copy(MSPGAUC[:,2]),
                       np.cumsum(MSPGAUC[:,0])
                      ]).T
MSPLCumGAUEQ=np.array([np.copy(MSPGAUEQ[:,2]),
                       np.cumsum(MSPGAUEQ[:,0])
                      ]).T
       
plt.xscale('log')
plt.xlim(0.002,6)
plt.xlabel('Radius(kpc)')
plt.yscale('log')
plt.tick_params(labelright=True)
plt.ylim([float(2*10**34), float(5*10**38)])
plt.ylabel('Cumulative Gamma Luminosity ($erg s^{-1}$)')
plt.plot(MSPLCumLONC[:,0],MSPLCumLONC[:,1],'k:', linewidth=2,label = 'MSP LON-C')
plt.plot(MSPLCumLONEQ[:,0],MSPLCumLONEQ[:,1],'k', linewidth=2,label = 'MSP LON-EQ')
plt.plot(MSPLCumGAUC[:,0],MSPLCumGAUC[:,1],'c:', linewidth=2,label = 'MSP GAU-C')
plt.plot(MSPLCumGAUEQ[:,0],MSPLCumGAUEQ[:,1],'c', linewidth=2,label = 'MSP GAU-EQ')

plt.legend(loc='upper left')
plt.title('Halo133-1.65e12msun CumuLum over Radius')
plt.savefig('pics/LumR_Hui133.png')
plt.show() 