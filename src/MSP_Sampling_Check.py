import numpy as np
import matplotlib.pyplot as plt

MSPEQSample=np.loadtxt('M31_F18_MSP_Dep_EQ_4.txt')
MSPCSample=np.loadtxt('M31_F18_MSP_Dep_C_4.txt')

Mrange=np.logspace(31,36,100)
plt.hist(MSPEQSample[:,0],bins=Mrange,label='EQ')
#plt.hist(MSPCSample[:,0],bins=Mrange,label='C')

plt.legend()
plt.xscale('log')
plt.show()