## Gamma-ray from millisecond pulsars (MSP) in globular clusters (GCs), a cosmological view

### modules
- Illustris (or IllustrisTNG) simulations
- Semi-analytical model of GC formation
- Iniital GC spatial distribution, orbital migration, dynamical friction, and mass loss
- Formation of nuclear star clusters
- Sample MSP within nuclear star clusters
- Gamma-rays from MSP

### Suggestions
- keep the source code in src folder, separate modules listed above into different files
- try to write functions rather than plan scripts since the codes need to be combined later
