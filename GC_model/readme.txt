﻿1.) put the parameters you want in “input.txt”. it has the following parameters: 

p2, p3: as in CGL18.

mpb:    if 0.0, run on the entire set of merger trees. if 1.0, only run on main branch.

Mc:     log10(Mcut/Msun) of Schechter function.

all:    if 1.0, run on all the merger trees we have in storage (~200; more can be downloaded from the Illustris database if needed). if 0.0, then use the next 3 params to run only on a subset of trees.

min:    log(min halo mass) you want to run the model on. only used if all = 0.0.

max:    log(max halo mass) you want to run the model on. only used if all = 0.0.

N:      number of halos you want to run the model on in the range (min, max). only used if all = 0.0.  



2.) run the model with “python main.py”. you will get a message when it’s done.


3.) the output will be in the form of two txt files: “all.txt” and “z0_cat.txt”. 
     The former has info on all clusters formed, the latter only has clusters that survive to z=0.
