
# coding: utf-8

# In[67]:

import matplotlib.pyplot as plt
from scipy import stats
from scipy.stats import chisquare
from scipy import interpolate
import numpy as np
import os

# Python code to calculate 1D Gaussian Mixture Modeling
# using Expectation-Maximization algorithm
# written by Oleg Gnedin on July 28, 2015

# load Python libraries and functions
from scipy import stats
def Expectation_Maximization(x, mu_, sig_, pk_, EqualVar=False, x_err=None):
    mu = np.copy(mu_); sig = np.copy(sig_); pk = np.copy(pk_);
    Kmodes = len(mu)
    logL = 999.
    logL1 = 99.
    iter = 0
    pnk = np.zeros([Kmodes,len(x)])
    e50 = np.zeros(Kmodes)

    while abs(logL-logL1) > 1.e-8:
        # E-step
        iter += 1
        pxn = np.zeros(len(x))
        for k in xrange(Kmodes):
            if x_err is None:
                s2 = np.square(sig[k])
            else:
                s2 = np.square(x_err) + np.square(sig[k])
            pxn += pk[k]/np.sqrt(2.*np.pi*s2)*np.exp(-np.square(x-mu[k])/s2/2.)

        logL1 = logL
        logL = np.sum(np.log(pxn))

        # M-step
        for k in xrange(Kmodes):
            if x_err is None:
                s2 = np.square(sig[k])
            else:
                s2 = np.square(x_err) + np.square(sig[k])            
            pnk[k] = pk[k]/np.sqrt(2.*np.pi*s2)*np.exp(-np.square(x-mu[k])/s2/2.)/pxn

        for k in xrange(Kmodes):
            if x_err is None:
                s2 = np.square(sig[k])
                e50[k] = 0.
            else:
                s2 = np.square(x_err) + np.square(sig[k])
                e50[k] = np.median(x_err[pnk[k]>=0.5])
            w = pnk[k]/s2
            sig2k = np.average(np.square(x-mu[k]), weights=w)
            sig[k] = np.sqrt(sig2k - e50[k]**2)
            mu[k] = np.average(x, weights=w)
            pk[k] = np.average(pnk[k])
            
        if(EqualVar):
            # set all variances to be equal
            s = np.mean(sig**2)
            sig[:] = np.sqrt(s)
        else:
            # constraint on deviation of the variances (Lo 2008)
            s = np.amax(sig)
            sig[sig < 0.25*s] = 0.25*s

    return {'mu':mu, 'sig':sig, 'pk':pk, 'pnk':pnk, 'logL':logL, 'iter':iter}


# AIC and BIC information criteria
def AIC(k, N, logL):
    return -2.*logL + 2.*k + 2.*k*(k+1)/(N-k-1)
    
def BIC(k, N, logL):    
    return -2.*logL + k*np.log(N*1.)


def run_GMM(data, mu_=[-1., 0.], x_err=None, nboot_nonpar=400, unimodal = False):
    mu = np.copy(mu_)
    sig = np.ones(len(mu))
    pk = np.ones(len(mu))/len(mu)
    np.random.seed(1)

    # basic statistics of the input dataset
    x = np.copy(data)
    kurt = stats.kurtosis(x)
    #print "Data: n = {0:d} mean = {1:.3f} std = {2:.3f} kurtosis = {3:.3f}"        .format(len(x), np.mean(x), np.std(x), kurt)
    
    # One mode: G1    
    # initial guess for the parameters
    mu1 = [0.]; sig1 = [1.]; pk1 = [1.];

    # find best parameters
    G1 = Expectation_Maximization(x, mu1, sig1, pk1, x_err=x_err)
    if(unimodal):
        return G1
    #print "Unimodal: mu = {0:.3f} sig = {1:.3f} logL = {2:.3f}"        .format(G1['mu'][0], G1['sig'][0], G1['logL'])
    
    # Multiple modes, different variances: Gdv
    Gdv = Expectation_Maximization(x, mu, sig, pk, x_err=x_err)

    #print "Different variances: iter={0:d}  logL = {1:.3f}".format(Gdv['iter'], Gdv['logL'])
    #for k in xrange(len(mu)):
    #    print "  mode {0:d}: n = {1:5.1f} p = {2:.3f} mu = {3:.3f} sig = {4:.3f}"        .format(k+1, len(x)*Gdv['pk'][k], Gdv['pk'][k], Gdv['mu'][k], Gdv['sig'][k])

    # separation of the peaks
    D = abs(Gdv['mu'][0]-Gdv['mu'][1])/np.sqrt((Gdv['sig'][0]**2 + Gdv['sig'][1]**2)/2.)
    #print "  D = {0:.2f}".format(D)
    if(not unimodal):
        return Gdv

    # analytical chi-square statistic (McLachlan 1987)
    chi2dv = 2.*(Gdv['logL']-G1['logL'])
    Ndof = 4*(len(mu)-1)
    #print "  Chi-square statistic (null=unimodal): d(chi2)={0:.2f} Ndof={1:d} p={2:8.2e}"        .format(chi2dv, Ndof, 1.-stats.chi2.cdf(chi2dv,Ndof))

    # Multiple modes, same variances: Gsv
    Gsv = Expectation_Maximization(x, mu, sig, pk, EqualVar=True, x_err=x_err)

    #print "Equal variances: iter={0:d}  logL = {1:.3f}".format(Gsv['iter'], Gsv['logL'])
    #for k in xrange(len(mu)):
    #    print "  mode {0:d}: n = {1:5.1f} p = {2:.3f} mu = {3:.3f} sig = {4:.3f}"        .format(k+1, len(x)*Gsv['pk'][k], Gsv['pk'][k], Gsv['mu'][k], Gsv['sig'][k])
    
    # separation of the peaks
    D = abs(Gsv['mu'][0]-Gsv['mu'][1])/np.sqrt((Gsv['sig'][0]**2 + Gsv['sig'][1]**2)/2.)
    #print "  D = {0:.2f}".format(D)

    # analytical chi-square statistic (McLachlan 1987)
    chi2 = 2.*(Gsv['logL']-G1['logL'])
    Ndof = 2*(len(mu)-1)
    #print "  Chi-square statistic (null=unimodal): d(chi2)={0:.2f} Ndof={1:d} p={2:8.2e}"        .format(chi2, Ndof, 1.-stats.chi2.cdf(chi2,Ndof))

    chi2 = 2.*(Gdv['logL']-Gsv['logL'])
    Ndof = 1
    #print "  Chi-square statistic (null=equal variance): d(chi2)={0:.2f} Ndof={1:d} p={2:8.2e}"        .format(chi2, Ndof, 1.-stats.chi2.cdf(chi2,Ndof))

    # AIC and BIC information criteria
    #print "Information criteria:"
    #print "1-mode: AIC={0:.1f} BIC={1:.1f}"        .format(AIC(2,len(x),G1['logL']), BIC(2,len(x),G1['logL']))
    #print "2-mode: AIC={0:.1f} BIC={1:.1f}"        .format(AIC(5,len(x),Gdv['logL']), BIC(5,len(x),Gdv['logL']))

    # Non-parametric Bootstrap to estimate errors of best parameters
    KK = len(mu)
    Kstat = 3*KK+3
    i=0
    not_converged = True
    G_boot = np.empty((nboot_nonpar, Kstat))
    Gstat = np.empty((2, Kstat))

    while not_converged and i < nboot_nonpar:
        # draw a bootstrap sample of the data
        x = data[np.random.randint(low=0, high=len(x), size=len(x))]
    
        # find best parameters
        G1 = Expectation_Maximization(x, mu1, sig1, pk1)
        G = Expectation_Maximization(x, mu, sig, pk)
    
        # assemble array of best parameters
        boot = (G['mu'], G['sig'], G['pk'])
        G_boot[i,:3*KK] = np.ndarray.flatten((np.array(boot)))
        G_boot[i,3*KK] = abs(G['mu'][0]-G['mu'][1])/np.sqrt((G['sig'][0]**2 + G['sig'][1]**2)/2.)
        G_boot[i,3*KK+1] = G1['mu'][0]
        G_boot[i,3*KK+2] = G1['sig'][0]
    
        # stop when the means and standard deviations vary little
        i += 1
        Gstat_l = np.copy(Gstat)
        for k in xrange(Kstat):
            Gstat[0,k] = np.mean(G_boot[:i,k]) 
            Gstat[1,k] = np.std(G_boot[:i,k])
        d = np.sum(abs(Gstat[0,:3*KK]-Gstat_l[0,:3*KK]) + abs(Gstat[1,:3*KK]-Gstat_l[1,:3*KK]))    
        if d < 0.001 and i >= 100:
            not_converged = False
    '''
    if nboot_nonpar > 0:
        print "Non-parametric bootstrap: number of steps = {0:d}  diff = {1:.4f}".format(i, d)
        print "  unimodal: mu = {0:.3f} +- {1:.3f} sig = {2:.3f} +- {3:.3f}"            .format(Gstat[0,-2], Gstat[1,-2], Gstat[0,-1], Gstat[1,-1]) 
        for k in xrange(len(mu)):
            print "  mode {0:d}: n = {1:5.1f} +- {2:.1f} mu = {3:.3f} +- {4:.3f} sig = {5:.3f} +- {6:.3f}"            .format(k+1, len(x)*Gstat[0,k+2*KK], len(x)*Gstat[1,k+2*KK],            Gstat[0,k], Gstat[1,k], Gstat[0,k+KK], Gstat[1,k+KK]) 
        print "  D = {0:.2f} +- {1:.2f}".format(Gstat[0,3*KK], Gstat[1,3*KK]) 

    '''
    # Parametric Bootstrap to rule out a unimodal distribution
    nboot = 200
    if len(x) > 1000:
        nboot = 0      # to prevent long run-time
    if len(x) < 20:
        nboot = 50     # to prevent numerical failures
    i=0
    ipar = np.zeros(3)
    not_converged = True

    while not_converged and i < nboot:
        # draw a bootstrap sample of the data
        x = np.random.normal(size=len(x))
    
        G1 = Expectation_Maximization(x, mu1, sig1, pk1)
        G = Expectation_Maximization(x, mu, sig, pk)
        i += 1
    
        # check if the bootstrap parameters are as large as observed
        if chi2dv <= 2.*(G['logL']-G1['logL']):
            ipar[0] += 1.
        if D <= abs(G['mu'][0]-G['mu'][1])/np.sqrt((G['sig'][0]**2 + G['sig'][1]**2)/2.):
            ipar[1] += 1.
        if kurt >= stats.kurtosis(x):  # note: kurtosis is negative
            ipar[2] += 1.
        if np.amin(ipar)/i > 0.01 and i >= 100:
            not_converged = False
        
    if nboot > 0:
        print("Parametric bootstrap to rule out a unimodal distribution: number of steps =",i)
        if ipar[0] >= 1.:
            print("p(chi2) = {0:.3f}".format(ipar[0]/i))
        else:
            print("p(chi2) < {0:.3f}".format(1./i))
        if ipar[1] >= 1.:
            print("p(D) = {0:.3f}".format(ipar[1]/i))
        else:
            print("p(D) < {0:.3f}".format(1./i))
        if ipar[2] >= 1.:
            print("p(kurt) = {0:.3f}".format(ipar[2]/i))
        else:
            print("p(kurt) < {0:.3f}".format(1./i))
    
    # output
    #x1 = x[pnk[0] > pnk[1]]
    #x2 = x[pnk[0] <= pnk[1]]
    return Gdv

'''

from scipy.interpolate import interp1d
f = '/Users/nchoksi/Google Drive/Astro/GC/Data/virgo_peaks_new.txt'
fout = open(f, 'w')
fout.write('#galID | logM* | blue peak  | red peak ([Fe/H]) | eq (100 if unimodally blue) | err_blue | err_red \n')
fin = '/Users/nchoksi/Google Drive/Astro/GC/Data/Virgo/VirgoGC_compiled3.txt'
hid, sm, met = np.loadtxt(fin, usecols = (0,1,3), unpack = True)
hid_unique = np.unique(hid)
for h in hid_unique:
    sm_h = sm[hid == h][0]
    met_h = met[hid == h]
    met_h = met_h[(met_h > -2.3) & (met_h < 0.3)]
    G = run_GMM(met_h, [-1.5, -0.5], x_err=None, nboot_nonpar=0, unimodal=False)
    mu_blue = G['mu'][0]
    mu_red = G['mu'][1]
    sig_blue = G['sig'][0]
    sig_red = G['sig'][1]

    if(mu_red < -1.0):  # all clusters are blue; use unimodal distribution instead
        G = run_GMM(met_h, [-1.5, -0.5], x_err=None, nboot_nonpar=0, unimodal=True)
        mu_blue = G['mu'][0]
        sig_blue = G['sig'][0]
        mu_red = 9999 # = unimodality 
        sig_red = 9999
        eq = 100; err_blue = 100; err_red = 100; #dummy values for now 

    if(sig_red != 9999): #if bimodal
        #find equality point 
        xt = np.linspace(-2, 0, num = 100000)
        yt1 = (sig_blue**-2)*np.exp(-(xt-mu_blue)**2 / (2*sig_blue**2))
        yt2 = (sig_red**-2)*np.exp(-(xt - mu_red)**2 / (2*sig_red**2))
        dyt = yt1-yt2
        f = interp1d( dyt, xt)
        try:
            eq = f(0)
            err_red = G['sig'][1]/np.sqrt(len(met_h))
        except ValueError:
            print "exception"
            continue
    err_blue = np.std(met_h)/np.sqrt(len(met_h)); 
    fout.write(str(h) + " " +  str(np.log10(sm_h)) + " " + str(mu_blue) + " " + str(mu_red) +   " " + str(eq) + " " + str(err_blue) + " " + str(err_red) + "\n")


Mv_sun = 4.83
def magToMass(Mv): 
    delta_M = Mv-Mv_sun
    luminosity = 10**(delta_M/-2.5) #L/L_sun
    #print "L = " , luminosity
    mass = 6*luminosity #assuming constant M/L ratio ~ 6 (applicable to BCGs)
    return mass
fout = open('/Users/nchoksi/gdrive/Astro/GC/Data/bcg_peaks.txt', 'w')
fout.write('#logSM | mu_blue | mu_red\n')
for fname in os.listdir('/Users/nchoksi/gdrive/Astro/GC/Data/BCG/metallicities'):  
    if(fname == '.DS_Store'):
        continue
    f = '/Users/nchoksi/gdrive/Astro/GC/Data/BCG/metallicities/' + fname
    z = np.loadtxt(f, unpack = True)

    mv = z[0]; sm = magToMass(mv);
    z = z[1:]*(1./0.924)
    z = z[(z > -2.3) & ( z < 0.3)]
    G = run_GMM(z, [-1.5, -0.5], x_err=None, nboot_nonpar=0, unimodal=False)
    err_blue = G['sig'][0]/np.sqrt(len(z)); err_red = G['sig'][1]/np.sqrt(len(z))
    mu_red = G['mu'][1]
    if(G['mu'][1] <= -1):
        G = run_GMM(z, [-1.5, -0.5], x_err=None, nboot_nonpar=0, unimodal=True)
        mu_red = 9999
    err_bcg = np.std(z, ddof = 1)/np.sqrt(len(z))
    fout.write(str(np.log10(sm)) + " " + str(G['mu'][0]) + " " + str(mu_red) +  " " + str(err_bcg) + "\n")
fout.close()
'''



