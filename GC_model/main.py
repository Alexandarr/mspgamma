import numpy as np
from scipy import interpolate
import time
import os
import sys
import schechter_interp
import smhm
np.random.seed(2)

#use same cosmology as Illustris
h100 = .704
omega_matter = 0.2726
omega_lambda = 1. - omega_matter
fb = .167 #cosmic baryon fraction


#model parameters, as defined in CGL18
mmr_slope = .35 #mass slope of galaxy mass-metallicity relation
mmr_turnover = 10.5 #pivot in the power-law of mass-metallicity relation
sigma_m = 0.3 #MMR scatter, in dex
max_feh = 0.3 #max [Fe/H]
sigma_gas = 0.3 #cold gas fraction scatter, in dex
log_Mmin = 5.0 #min cluster mass to draw from CIMF
tdep = 0.3 #scaling of the gas depletion time with redshift, tdep \propto (1+z)^(-alpha), alpha as here
mmr_evolution = .9 #redshift slope of the galaxy mass-metallicity relation
pr = 0.5 #normalized period of rotation; t_tid \propto P
miso = (pr/1.7)**3 #mass where tiso < ttid




#mainpath = '/Users/nchoksi/gdrive/Astro/GC/Model/'
mainpath = '/Users/hliastro/Dropbox_MIT/GC_pulsar_gammaray/mspgamma/GC_model/'




fm = open(mainpath + 'massloss.txt')
flost, t_solar, t_subsolar = np.loadtxt(fm, usecols = (1,2,3), unpack = True)

ssub = interpolate.interp1d(t_subsolar, flost, kind = 'linear')
ssolar = interpolate.interp1d(t_solar, flost, kind = 'linear')


snaps2 = np.loadtxt(mainpath + "snaps.txt", unpack = True)

#treedir = mainpath + 'SublinkTrees'
treedir = mainpath + "fixed_trees_large_spin/"


cat = open(mainpath + 'z0_cat.txt', 'w') #full catalog of all surviving GCs
allcat = open(mainpath + 'all.txt', 'w') #full catalog of all GCs

input_file = mainpath + "input.txt"
p2, p3, mpb_only, mc, run_all, log_mh_min, log_mh_max, N = np.loadtxt(input_file, unpack = True)
mpb_only = bool(mpb_only)
run_all = bool(run_all)

cat.write('#model parameters: p2, p3, mpb_only, Mc = ' +  str(p2) + " " +  str(p3) +  " " + str(mpb_only) + " " +  str(mc)  + "\n")
cat.write(str("#haloID") + " | " + str('logMh(z=0') + " | " +  str("logM*(z=0)") + " | " + str('logMh(zform)') + " | " + str("logM*(zform)") + " | " + str("logM(z=0)") + " | " + str("logM(zform)") +  " | " + str("zform") + " | " + str("feh") + " | " + str("isMPB") + " | " + str("halo ID @ formation") + "\n")

allcat.write('#model parameters: p2, p3, mpb_only, Mc = ' +  str(p2) + " " +  str(p3) +  " " + str(mpb_only) + " " +  str(mc)  + "\n")
allcat.write("#haloID | logMh(z=0) | haloID @ form | logMh(tform) | logM*(tform) | logMgas(tform) | logMcl(tform) | zform | [Fe/H] \n")

#initialize all the interpolation tables for use with Schechter function
schechter_interp.init(10**mc)
mgc_to_mmax = schechter_interp.generate(10**mc)
alpha = -2.0
ug52 = schechter_interp.upper_gamma2(5.0)

# Define globular cluster class to store data about each cluster.
class GC :
    def __init__(self, mass, originHaloMass, origin_redshift, metallicity, osm, omgas, is_mpb, idform) :
        self.mass = mass
        self.originHaloMass = originHaloMass
        self.origin_redshift = origin_redshift
        self.metallicity = metallicity
        self.origin_sm = osm
        self.origin_mgas = omgas
        self.is_mpb = is_mpb
        self.idform = idform
# Calculate gas mass given stellar mass, halo mass, redshift using scaling relations. Double power law for SM-Mg relation, then scale with redshift. Revise if gas fraction exceeds accreted baryon fraction. As described in Choksi, Gnedin, and Li (2018).
def gasMass(SM, Mh, z) :
    slope = 0.33
    if(SM < 1e9):
        slope = 0.19
    log_ratio = 0.05 - 0.5 -  slope*(np.log10(SM) - 9.0) #log10(Mg/M*)
    if(z < 3): #fg saturates at z > 3
        if(z < 2):
            log_ratio += (3.0-tdep)*np.log10((1.+z)/3.) + (3.0-tdep)*np.log10(3.) #strong ssfr evolution at z < 2
        else:
            log_ratio += (1.7-tdep)*np.log10((1.+z)/3.) + (3.0-tdep)*np.log10(3.) #weak ssfr evolution at z > 2 (Lilly+)
    else:
        log_ratio += (1.7-tdep)*np.log10((1.+3)/3.) + (3.0-tdep)*np.log10(3.) #weak ssfr evolution at z > 2
    log_ratio += np.random.normal(0, 0.3)
    ratio = 10**log_ratio
    Mg = SM*ratio
    fstar = SM/(fb*Mh)
    fgas = Mg/(fb*Mh)

    Mc = 3.6e9*np.exp(-.6*(1+z))/h100 #cutoff mass
    Mc_min = 1.5e10*(180**(-.5))/(smhm.E(z)*h100)
    if(Mc < Mc_min) :
        Mc = Mc_min
    fin = 1/((1+(Mc/Mh))**3)

    if(fstar+fgas > fin):
        fgas = fin-fstar
        Mg = fgas*fb*Mh
    return Mg
# Galaxy stellar mass-metallicity relation, with additional redshift evolution, as described in Choksi, Gnedin, and Li (2018).
def MMR(SM, z) :
    local = mmr_slope*(np.log10(SM) - mmr_turnover)
    evolution = mmr_evolution*np.log10(1+z)
    fe_h = local - evolution
    #metallicity saturates at slightly supersolar values
    if(fe_h > max_feh):
        fe_h = max_feh
    return fe_h
def clusterFormation(Mg, halomass, redshift, metallicity, SM, is_mpb, hid) :
    gc_list = []
    Mgc = 3e-5*p2*Mg/fb #total mass of all GCs formed in cluster formation event
    log_Mgc = np.log10(Mgc)
    if(log_Mgc < log_Mmin): #not enough mass to form a single cluster of mass Mmin
        return gc_list
    #calculate the cumulative distribution r(<M), and invert it numerically
    log_Mmax = mgc_to_mmax(log_Mgc)
    if(log_Mmax > log_Mgc):
        log_Mmax = log_Mgc
    Mmax = 10**log_Mmax
    mt = np.logspace(log_Mmin, log_Mmax, num = 500)

    nuclear_cluster = GC(Mmax, halomass, redshift, metallicity + np.random.normal(0,sigma_m), SM, Mg, is_mpb, hid)
    gc_list.append(nuclear_cluster)
    mass_sum = Mmax

    ntot = ug52 - schechter_interp.upper_gamma2(log_Mmax)
    cum = np.array([(ug52 - schechter_interp.upper_gamma2(np.log10(mv)))/ntot for mv in mt])

    r_to_m = interpolate.interp1d(cum, mt)
    mass_sum2 = Mmax
    while(mass_sum < Mgc):
        r = np.random.random()
        M = r_to_m(r)
        if(mass_sum+M > Mgc): #make sure the final cluster drawn doesn't exceed the total mass to be formed. it may produce some clusters below Mmin, but shouldn't really matter (will disrupt)
            M = Mgc-mass_sum
        mass_sum += M
        cluster = GC(M, halomass, redshift, metallicity+np.random.normal(0,sigma_m), SM, Mg, is_mpb, hid)
        gc_list.append(cluster)
    return gc_list
#metallicity dependent stellar evolution, as calculated by Prieto & Gnedin
def massFraction(fe_h, t_alive):
    if(fe_h >= 0 ): #supersolar metallicity
        return ssolar(t_alive)
    elif(fe_h <= -0.69): #if Z<= 0.2Zsun
        return ssub(t_alive)
    else: #intermediate case: interpolate (in log-space)  between .2*Z_sun and Z_sun
        slope = (t_solar - t_subsolar)/0.69 #log10(0.2) = -0.69
        t_int= t_subsolar + slope*(fe_h - -0.69) #new intermediate MS lifetime table
        sint = interpolate.interp1d(t_int, flost, kind = 'linear')
        return sint(t_alive)
#disruption prescription as described in CGL18
def disruption(M0, fe_h, origin_redshift, tnow, use_weak = False): #if use_weak = True, allow for disruption in weak tidal field limit (see CGL18 for details)
    m0 = M0/2e5
    tform = smhm.cosmicTime(origin_redshift, units = 'yr')
    t_tid0 = 1e10*m0**(2./3)*pr
    nu_tid0 = 1./t_tid0

    k = 1-(2./3)*nu_tid0*(tnow - tform)
    k = max(k, 0) #can't raise negative number to fractional exponent; if 1-k < 0, then m_tid(z=0) < miso, so just set m_tid(z=0) = 0
    mtid_z0 = m0*k**1.5

    if(use_weak): #use dM/dt = -M/min(t_tid, t_iso), as in CGL18
        if(mtid_z0 >= miso):
            mf = mtid_z0
        else:
            tiso = tform + 3.0*(1 - (miso/m0)**(2./3))/(2*nu_tid0)
            mf = miso - (tnow - tiso)/(1.7e10)
        if(mf <= 0):
            return 0

    f_lost = massFraction(fe_h, tnow - tform) #stellar evolution
    if(use_weak):
        return mf*2e5*(1-f_lost)
    else:
        return mtid_z0*2e5*(1-f_lost)
def loadTree(fname):
    m, fp, subid, mpi, redshifts = [], [], [], [], []
    with open(treedir + fname ) as f:
        count = -1
        for line in f:
            count += 1
            if(count == 0):
                continue
            cols = line.split()
            m.append(float(cols[0])); fp.append(long(cols[1])); subid.append(long(cols[2])); mpi.append(long(cols[3])); redshifts.append(float(cols[5]));
    m  = np.array(m); fp  = np.array(fp); subid = np.array(subid); mpi = np.array(mpi); redshifts = np.array(redshifts);
    mpbi = mpi[m == np.amax(m)][0]
    if(mpb_only):
        m = m[mpi == mpbi]; fp = fp[mpi == mpbi]; subid = subid[mpi == mpbi]; redshifts = redshifts[mpi == mpbi];mpi = mpi[mpi == mpbi]
    m = 10**m
    msub = np.amax(m)
    return m, fp, subid, redshifts, mpi, msub, mpbi


t0 = smhm.cosmicTime(0.0, units = 'yr')
#id_dict = loadDict()


num = -1
num_run = 0
for fname in os.listdir(treedir):
    if(fname == ".DS_Store"):
        continue

    m, fp, subid, redshifts, mpi, msub, mpbi = loadTree(fname)
    if(run_all == False and msub  < 10**log_mh_min or msub > 10**log_mh_max or num_run >= N):
        continue
    num_run += 1

    # Go through each halo along the tree and look for events satisfying Rm > p3.
    sm_arr = np.zeros(len(redshifts))
    clusters = []
    for i in range(0, len(m)) : #for each halo in the merger tree
        mass = m[i] #mass of this halo
        fpID = fp[i] #ID of the main progenitor

        if(fpID == -1 or len(subid[subid == fpID]) == 0): #then we've reached the first point along this track of the tree
            sm_arr[i] = smhm.SMHM(mass, redshifts[i], scatter = True) #assign a "seed" stellar mass which we will grow self-consistently
            continue

        progIdx = np.where(subid == fpID)[0][0] #identify index of first progenitor in data
        progMass = m[progIdx] #get mass of fprogenitor

        ratio = mass/progMass - 1 #calculate merger ratio, Rm = dMh/Mh
        znow, zbefore = redshifts[i], redshifts[progIdx]
        dt = smhm.cosmicTime(znow, units = 'Gyr')- smhm.cosmicTime(zbefore, units = 'Gyr')
        ratio = ratio/dt #(dMh/Mh)/dt

        #evolve stellar mass self-consistently as described in CGL18
        sm1 = smhm.SMHM(mass, znow, scatter = False); sm2 = smhm.SMHM(progMass, zbefore, scatter = False)
        dsm = sm1-sm2
        a = 1./(1+znow)
        scatter = np.random.normal(0,.218 - .023*(a-1))
        SM = sm_arr[progIdx] + dsm*10**scatter
        if(SM < 0): #only happens in very weird cases at very high redshift
            SM = sm_arr[progIdx]
        sm_arr[i] = SM
        Mg = gasMass(SM, mass, znow)
        if(ratio > p3):  #if merger criterion satisfied
            metallicity = MMR(SM, znow)
            is_mpb = mpi[i] == mpbi
            clusters.extend(clusterFormation(Mg, mass, znow, metallicity, SM, is_mpb, subid[i]))
            continue

    clusters2, log_initial_masses = [], []
    for cluster in clusters:
        final_mass = disruption(cluster.mass, cluster.metallicity, cluster.origin_redshift, t0)
        if(final_mass > 0):
            log_initial_masses.append(np.log10(cluster.mass))
        clusters2.append(GC(final_mass, cluster.originHaloMass, cluster.origin_redshift, cluster.metallicity, cluster.origin_sm, cluster.origin_mgas, cluster.is_mpb, cluster.idform))

    evolved_clusters = [cluster for cluster in clusters2 if cluster.mass > 0]

    #all GCs that form, regardless of survival -- for use w/ allcat.txt
    GC_mets = np.array([cluster.metallicity for cluster in clusters]); GC_masses = np.array([cluster.mass for cluster in clusters]); GC_log_masses = np.log10(GC_masses); GC_redshifts = np.array([cluster.origin_redshift for cluster in clusters])
    GC_idform = np.array([cluster.idform for cluster in clusters]); GC_mhost_tform = np.array([cluster.originHaloMass for cluster in clusters])
    GC_log_mhost_tform = np.log10(GC_mhost_tform); GC_log_mstar_tform = np.round(np.log10(np.array([cluster.origin_sm for cluster in clusters])), 3)
    GC_log_mgas_tform = np.round(np.log10(np.array([cluster.origin_mgas for cluster in clusters])), 3)

    #again, only surviving clusters this time
    GC_masses2 = np.array([cluster.mass for cluster in evolved_clusters]); GC_metallicity2 = np.array([cluster.metallicity for cluster in evolved_clusters])
    GC_redshifts2 = np.array([cluster.origin_redshift for cluster in evolved_clusters]); GC_mhost_tform2 = np.array([cluster.originHaloMass for cluster in evolved_clusters])
    GC_mstar_tform2 = np.array([cluster.origin_sm for cluster in evolved_clusters]); GC_ismpb2 = np.array([cluster.is_mpb for cluster in evolved_clusters])
    GC_idform2 = np.array([cluster.idform for cluster in evolved_clusters]); log_gc_masses2 = np.log10(GC_masses2)


    logmsub = np.log10(msub);
    log_GC_mhost =  np.empty(len(GC_masses2))
    log_GC_mhost.fill(logmsub)
    logms = np.log10(smhm.SMHM(10**logmsub, 0.0, scatter = False))

    #write to output files
    hid_num = int(fname[0:fname.find(".")])
    for i in range(len(GC_masses)): #all clusters
        allcat.write(str(hid_num) + " " + str(np.round(logmsub,5)) + " " + str(GC_idform[i]) + " " + str(np.round(GC_log_mhost_tform[i],5)) + " " + str(np.round(GC_log_mstar_tform[i],5)) + " " + str(np.round(GC_log_mgas_tform[i],5)) + " " + str(np.round(GC_log_masses[i],5)) + " " + str(np.round(GC_redshifts[i],5)) + " "  + str(np.round(GC_mets[i],5)) + "\n")
    for i in range(len(GC_masses2)): #only those surviving to z=0
        zform = GC_redshifts2[i]
        mh_tform = GC_mhost_tform2[i]; log_mh_tform = np.log10(mh_tform);
        sm_tform = GC_mstar_tform2[i]; log_sm_tform = np.log10(sm_tform);
        logm = log_gc_masses2[i]; logm_tform = log_initial_masses[i];
        cat.write(str(hid_num) +  " " + str(np.round(logmsub,5)) + " " + str(np.round(logms, 5)) + " " +  str(np.round(log_mh_tform, 5)) + " " + str(np.round(log_sm_tform,5)) + " " +  str(np.round(logm,5)) + " " + str(np.round(logm_tform,5)) + " " + str(np.round(zform, 5)) + " " + str(np.round(GC_metallicity2[i],5)) + " " + str(float(GC_ismpb2[i])) +  " " + str(GC_idform2[i])  + "\n")
cat.close()
allcat.close()
if(num_run < N and run_all == False):
    print "requested " , N , " halos, but there were only " , num_run , " halos stored in the mass range you requested. Model was run on all available halos.\n"
print "all done!"







