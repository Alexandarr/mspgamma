import numpy as np
import os
#assign a unique ID to each original merger tree file

#f = '/users/nchoksi/gdrive/astro/gc/model/Sublink_Large/'
f = '/u/home/nickchok/analytic_model/Sublink_Large/'
fout = open('/u/home/nickchok/analytic_model/id_lookup_large.txt', 'w')
c = 0
for fname in os.listdir(f):
	if(fname == ".DS_Store"):
		continue
	fout.write(str(c) + ","  + fname + "\n")
	c += 1
fout.close()
