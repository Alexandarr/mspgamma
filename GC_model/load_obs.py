import matplotlib.pyplot as plt
import matplotlib
from matplotlib import rcParams
from scipy import stats
from scipy.stats import chisquare
from scipy import interpolate
import numpy as np
import os
import sys
#sys.path.insert(0, '/users/nchoksi/gdrive/astro/gc/code/')
import gmm_output
import smhm
from scipy.interpolate import interp1d
Mv_sun = 4.83 #V-band absolute magnitude of the Sun
minz = -2.3; maxz = 0.3;
#data_dir = '/users/nchoksi/gdrive/astro/gc/data/'
data_dir = '/u/home/nickchok/analytic_model/data/'



def histedges_equalN(x, nbin):
    npt = len(x)
    return np.interp(np.linspace(0, npt, nbin + 1), np.arange(npt), np.sort(x))

def binData(x,y, numbins, p, add = True, eqn = False, minx = None, maxx = None): #return interquartile range for x,y in bins
	
	if(eqn):
		n, bins = np.histogram(x, bins = histedges_equalN(x, numbins))
	else:
		n, bins = np.histogram(x, bins = numbins)

	xbin1, yp = [], []
	for i in range(0, len(bins)-1):
		c = (x >= bins[i]) & (x < bins[i+1])
		xbin1.append(np.average(x[c]))
		yp.append(np.percentile(y[c], p))
	if(add):
		if(minx is None):
			minx = np.amin(x)
		if(maxx is None):
			maxx = np.amax(x)
		xbin, yp = addEnds(xbin1, yp, np.amin(x), np.amax(x))
	return np.array(xbin), np.array(yp)
#add end points to binned data by linearly interpolating down/up to minx, maxx
def addEnds(x,y, minx, maxx): #x and y are values after binning
    slope_low = (y[1] - y[0])/(x[1] - x[0])
    slope_high = (y[-1] - y[-2])/(x[-1] - x[-2])
    
    y_low = y[0] + slope_low*(-x[0] + minx)
    y_high = y[-1] + slope_high*(-x[-1] + maxx)
    
    y = np.insert(y, 0,  y_low)
    y = np.append(y, y_high)

    x = np.insert(x, 0, minx)
    x = np.append(x, maxx)
    
    return x, y
def observedData(dz, sigma1, sigma2, p): #return stdev and avg of combined gaussian (for use w/ Harris+ 15 Table 4)
    mets = np.array([])
    mu_mp = -1.15 #Harris only gives the separation of the peaks. so we will assume a constant peak of blue GCs of -1.15
    
    #generatea  sample, then calculate the mean and dispersion
    num = 1000000
    num_mp = int(p*num)
    num_mr = num-num_mp # = num*(1 - p)
    mets = np.append(mets, np.random.normal(mu_mp,sigma1, size=num_mp))
    mets = np.append(mets, np.random.normal(mu_mp+dz,sigma2, size=num_mr))
    mets = mets[(mets > minz) & (mets < maxz)]
    return np.std(mets, ddof = 1), np.average(mets)
def magToMass(Mv, ml = 6): 
    delta_M = Mv-Mv_sun
    luminosity = 10**(delta_M/-2.5) #L/L_sun
    mass = ml*luminosity #assuming constant M/L ratio ~ 6 (applicable to BCGs, as determined from lots of NED searching)
    return mass

def main():





	#observed data for BCGs: Table 4 from Harris+ 2015. parameters for bimodal fit to BCG metallicity distributions
	#use these parameters to recover the values that we want: sigma_[Fe/H] and mean [Fe/H]
	p1 = np.array([.57, .54, .63, .33, .49, .52, .48, .51, .35])
	sigma1 = np.array([.39, .24, .23, .28, .2, .23, .19, .26, .28])
	sigma2 = np.array([.56, .29, .36, .39, .42, .38, .45, .45, .42])
	dz = np.array([1.01, .91, .94, 1.18, 1.09, .98, .93, 1, 1.13])
	mv = np.array([-23.7, -22.42, -22.02, -22.35, -22.13, -21.87, -21.96, -23.31, -22.35])
	#calculate stdev and avgs using sum of two gaussians
	stdev_obs = np.array([])
	avg_obs = np.array([])
	std_err = np.zeros(len(p1)); avg_err = np.zeros(len(p1)) #don't have full sample so can't compute these values
	for dzv, sigma1v, sigma2v, p1v in zip(dz, sigma1, sigma2, p1):
	    s, a = observedData(dzv, sigma1v, sigma2v, p1v)
	    stdev_obs = np.append(stdev_obs, s)
	    avg_obs = np.append(avg_obs, a)
	masses_obs = magToMass(mv)                   


	#using full data for BCGs for galaxies in Harris+ 14
	dir_full = data_dir + 'BCG/metallicities/'
	avg_bcg14, std_bcg14, sm_bcg14 = np.array([]), np.array([]), np.array([])
	avg_err_bcg14 = np.array([]); std_err_bcg14 = np.array([])
	for fname in os.listdir(dir_full):
	    if(fname ==  ".DS_Store" or fname == "PARAMETERS.rtf" or fname == 'Icon'): 
	        continue
	    z = np.loadtxt(dir_full + fname, unpack = True)
	    mag = z[0] #first line in these files is Mv of galaxy
	    z = z[1:] #metallicities, get rid of Mv
	    z = z[(z > minz) & (z < maxz)]
	    sm_bcg14 = np.append(sm_bcg14, magToMass(mag))
	    avg_bcg14 = np.append(avg_bcg14, np.average(z))
	    s = np.std(z, ddof = 1); 
	    n = len(z);
	    std_bcg14 = np.append(std_bcg14, s)
	    avg_err_bcg14 = np.append(avg_err_bcg14, s/np.sqrt(len(z)))
	    std_err_bcg14 = np.append(std_err_bcg14, s/np.sqrt(2*(n-1)))

	#observed data for VCS
	hostID, hostSM, hostHM, gcZ, gcM = np.loadtxt( data_dir + 'Virgo/VirgoGC_compiled_new.txt', usecols = (0,1,2,3,4), unpack = True)
	uniqueID = np.unique(hostID)
	hm_virgo, sm_virgo, avgZ_virgo, stdZ_virgo = np.array([]), np.array([]), np.array([]), np.array([])
	std_err_virgo = np.array([]); avg_err_virgo = np.array([]);
	for i in range(0, len(uniqueID)): 
	    c = hostID == uniqueID[i]
	    if(len(hostID[c]) < 10): #minimum number of GCs for analysis
	        continue
	        
	    #get hm, sm, Z for the current galaxy        
	    sm = hostSM[c][0]
	    if(sm < 1e9):
	        continue
	    hm_virgo = np.append(hm_virgo, hostHM[c][0])
	    sm_virgo = np.append(sm_virgo, sm)
	    mets = gcZ[c]
	    mets = mets[(mets < maxz) & (mets > minz)] #filter out abnormally high metallicities 
	    avgZ_virgo = np.append(avgZ_virgo, np.average(mets))
	    s = np.std(mets, ddof = 1)
	    stdZ_virgo = np.append(stdZ_virgo, s)
	    n = len(mets)
	    avg_err_virgo = np.append(avg_err_virgo, s/np.sqrt(n))
	    std_err_virgo = np.append(std_err_virgo, s/np.sqrt(2*(n-1)))


	#metallicity calibration to scale to VCS calibrations

	#first calibrate tables to VCS using M87
	std_bcg =  stdev_obs[1] #M87 is 1th entry
	z_m87_virgo = (gcZ[hostID == 1316]) #1316 == VCS id of M87
	z_m87_virgo = z_m87_virgo[(z_m87_virgo > minz) & (z_m87_virgo < maxz)]
	std_virgo = np.std(z_m87_virgo, ddof = 1)
	#std_virgo = iqr(z_m87_virgo) #CHANGE HERE
	print("Std of M87 using Table 4 ", std_bcg)
	print("Std of M87 from VCS ", std_virgo)
	f1 = std_bcg/std_virgo
	print("f1: std_table / std_VCS (using M87):" , f1)
	#calibrate full BCG data to tables
	#now compare ngc 6166
	std_ngc6166_bcg14 = std_bcg14[3]
	std_ngc6166_bcg15 = stdev_obs[0]
	print("Std of NGC 6166 using full BCG data ", std_ngc6166_bcg14)
	print("Std of NGC 6166 using Table 4 ", std_ngc6166_bcg15)
	f2 =  std_ngc6166_bcg15/std_ngc6166_bcg14

	print("f2: std_table / std_bcg (using NGC 6166):" , f2)
	print("std_bcg / std_VCS = f1/f2: ", f1/f2)


	#add obs data for MW and M31

	f = data_dir + 'GC_MW.txt'
	z, mass = np.loadtxt(f, unpack = True, usecols = (0,1) )
	f = data_dir + 'GC_M31.txt'
	z31 = np.loadtxt(f, usecols = [4], unpack = True)

	z = z[(z >= minz) & (z <= maxz)]
	z31 = z31[(z31 >= minz) & (z31 <= maxz)]

	std_mw = np.std(z, ddof = 1)
	std_m31 = np.std(z31, ddof = 1)

	std_err_mw = std_mw/np.sqrt(2*(len(z)-1))
	std_err_m31 = std_m31/np.sqrt(2*(len(z31) -1))

	avg_mw = np.average(z)
	avg_m31 = np.average(z31)

	avg_err_mw = std_m31/np.sqrt(len(z))
	avg_err_m31 = std_m31/np.sqrt(len(z31))


	mass_mw = 10**10.78 #Licquia and Newman 2014
	mass_m31 = 10**11.0 #Tamm+ 2012, from SED modeling

	#and a few LG dwarves. veljanoski+...
	f = data_dir + 'ngc147.txt'
	z147 = np.loadtxt(f, unpack = True)
	f = data_dir + 'ngc185.txt'
	z185 = np.loadtxt(f, unpack = True)
	f = data_dir + 'ngc6822.txt'
	z6822 = np.loadtxt(f, unpack = True)



	#compile all observed data into giant arrays
	#standard deviations
	std_allobs = []
	std_allobs = np.append(std_allobs, stdZ_virgo)
	std_allobs = np.append(std_allobs, np.delete(stdev_obs/f1, 1)) #delete M87 repeat from Table 4 data
	std_allobs = np.append(std_allobs, np.delete(std_bcg14/(f1/f2), 3)) #delete ngc6166 from full bcg data
	std_allobs = np.append(std_allobs, [std_mw, std_m31])
	#means
	avg_allobs = np.array([])
	avg_allobs = np.append(avg_allobs, avgZ_virgo )
	avg_allobs = np.append(avg_allobs, np.delete(avg_obs/f1, 1))
	avg_allobs = np.append(avg_allobs, np.delete(avg_bcg14/(f1/f2), 3))
	avg_allobs = np.append(avg_allobs, [avg_mw, avg_m31])
	#stellar masses
	sm_allobs = np.array([])
	sm_allobs = np.append(sm_allobs, sm_virgo)
	sm_allobs = np.append(sm_allobs, np.delete(masses_obs, 1))
	sm_allobs = np.append(sm_allobs, np.delete(sm_bcg14,3))
	sm_allobs = np.append(sm_allobs, np.array([mass_mw, mass_m31]))
	logsm_allobs = np.log10(sm_allobs)
	#errors on dispersions
	std_err_allobs = []
	std_err_allobs = np.append(std_err_allobs, std_err_virgo)
	std_err_allobs = np.append(std_err_allobs, np.delete(std_err/f1, 1)) #delete M87 repeat from Table 4 data
	std_err_allobs = np.append(std_err_allobs, np.delete(std_err_bcg14/(f1/f2), 3)) #delete ngc6166 from full bcg data
	std_err_allobs = np.append(std_err_allobs, [std_err_mw, std_err_m31])
	#errors on means
	#errors on means
	avg_err_allobs = []
	avg_err_allobs = np.append(avg_err_allobs, avg_err_virgo)
	avg_err_allobs = np.append(avg_err_allobs, np.delete(avg_err/f1, 1)) #delete M87 repeat from Table 4 data
	avg_err_allobs = np.append(avg_err_allobs, np.delete(avg_err_bcg14/(f1/f2), 3)) #delete ngc6166 from full bcg data
	avg_err_allobs = np.append(avg_err_allobs, [avg_err_mw, avg_err_m31])

	logsm_allobs = np.append(logsm_allobs, [])


	mt = np.logspace(10.5,16, num = 5000)
	sm_k = np.array([smhm.SMHM(m, 0, k = True) for m in mt])
	sm_to_hm = interpolate.interp1d(np.log10(sm_k),np.log10(mt)) #interpolate backwards to get stellar mass
	logmh_allobs = sm_to_hm(logsm_allobs) 
	mh_allobs = 10**logmh_allobs

	return logmh_allobs, logsm_allobs, avg_allobs, std_allobs, avg_err_allobs, std_err_allobs


def phase_diag_contour_mass_level(X, Y, N_bin, levels, weight=None): #for age-metallicity plot 
    """
    plot the phase diagram of physical quantity X and Y.
    The weights of each point, e.g. cell mass, are needed to be specified
    in order to calculate the contour lines, within which fractions of mass
    are enclosed.
    """
    fig, ax = plt.subplots(figsize = (6,6))
    if weight!=None:
        result = np.histogram2d(X, Y, bins=N_bin, weights=weight)
    else:
        result = np.histogram2d(X, Y, bins=N_bin)
    N_matrix = result[0]
    x_c = (result[1][0:-1]+result[1][1:])/2.
    y_c = (result[2][0:-1]+result[2][1:])/2.
    weight_sort = np.sort(np.reshape(N_matrix, (N_matrix.size)))
    weight_cum = np.cumsum(weight_sort)
    f = interp1d(weight_cum, weight_sort)
    weight_level = np.array([f((1.0-l)*weight_cum[-1]) for l in levels])
    
    #plt.scatter(X,Y,s=1, alpha=0.1, c=weight)
    
    X_mesh, Y_mesh = np.meshgrid(x_c, y_c)

    cs = plt.contourf(X_mesh, Y_mesh, N_matrix.T, levels=weight_level, cmap = plt.cm.Greys, alpha = 0.4, zorder = 1)
    cs = plt.contour(X_mesh, Y_mesh, N_matrix.T, levels=weight_level, colors = 'k', alpha = 1.0, zorder = 1)
    fmt = {}
    strs = np.array(["{0:.0f}%".format(l*100) for l in levels])
    for l,s in zip( cs.levels, strs ):
        fmt[l] = s
    plt.clabel(cs, cs.levels, fmt=fmt, fontsize=15 , zorder = 1)
    plt.legend(loc = 3, frameon = False, numpoints = 2)
    return fig, ax



def generatePeaks(fin, metcut = False):
    hid, met  = np.loadtxt(fin, unpack = True, usecols = (0,8))
    uid = np.unique(hid)
    
    hid_peaks, blue_peaks, red_peaks, eqv_peaks, err = [], [], [], [], []
    a, b = 0, 0
    met_uni = np.array([])
    xt = np.linspace(-2.5, -0.3, num = 500000)
    for hv in uid:
        met_hv = met[hid == hv]
        if(len(met_hv) <= 5):
            continue
        G = gmm_output.run_GMM(met_hv, [-1.5, -0.5], x_err=None, nboot_nonpar=0, unimodal=False)
        mu1 = min(G['mu']); mu2 = max(G['mu']); 
        if(mu1 == G['mu'][0]):
            sig1 = G['sig'][0]; p1 = G['pk'][0]
            sig2 = G['sig'][1]; p2 = G['pk'][1]

        else:
            sig1 = G['sig'][1]; p1 = G['pk'][1]
            sig2 = G['sig'][0]; p2 = G['pk'][0]
        if(mu2 < -1.0): #if the MR peak is at [Fe/H] < -1.0, then we will instead model the distribution as a single ("blue") gaussian
            met_uni = np.append(met_uni, met_hv)
            a += 1
            G = gmm_output.run_GMM(met_hv, [-1.5, -0.5], x_err=None, nboot_nonpar=0, unimodal= True)
            hid_peaks.append(hv); blue_peaks.append(G['mu'][0]); red_peaks.append(9999); eqv_peaks.append(9999); #this should ensure that all clusters are treated as blue for unimodal case
        else:
            try:
                yt1 = (p1/sig1)*np.exp(-(xt-mu1)**2 / (2*sig1**2))
                yt2 = (p2/sig2)*np.exp(-(xt - mu2)**2 / (2*sig2**2))
                dyt = yt1-yt2
                f = interpolate.interp1d(dyt, xt)
                eqv = f(0)
                hid_peaks.append(hv); blue_peaks.append(G['mu'][0]); red_peaks.append(G['mu'][1]); eqv_peaks.append(eqv);
            except ValueError:
                b += 1
                eqv = -0.9 #if can't find minimum, just set equality to -0.9
                hid_peaks.append(hv); blue_peaks.append(mu1); red_peaks.append(mu2); eqv_peaks.append(eqv); 
        err.append(np.std(met_hv, ddof = 1)/np.sqrt(len(met_hv)))
    print("unimodal, number of bimodals w/o determined eqvs: ", a, b)
    return np.array(hid_peaks), np.array(blue_peaks), np.array(red_peaks), np.array(eqv_peaks), np.array(err)

def gasMass(SM, Mh, z) : 
    fb = 0.167
    tdep = 0.3
    slope = 0.33
    h100 = 0.704
    if(SM < 1e9):
        slope = 0.19
    log_ratio = 0.05 - 0.5 -  slope*(np.log10(SM) - 9.0) #log10(Mg/M*)
    if(z < 3): #fg saturates at z > 3
        if(z < 2): 
            log_ratio += (3.0-tdep)*np.log10((1.+z)/3.) + (3.0-tdep)*np.log10(3.) #strong ssfr evolution at z < 2
        else:
            log_ratio += (1.7-tdep)*np.log10((1.+z)/3.) + (3.0-tdep)*np.log10(3.) #weak ssfr evolution at z > 2 (Lilly+)
    else:
        log_ratio += (1.7-tdep)*np.log10((1.+3)/3.) + (3.0-tdep)*np.log10(3.) #weak ssfr evolution at z > 2 
    ratio = 10**log_ratio
    Mg = SM*ratio
    fstar = SM/(fb*Mh)
    fgas = Mg/(fb*Mh)
    
    Mc = 3.6e9*np.exp(-.6*(1+z))/h100 #cutoff mass
    Mc_min = 1.5e10*(180**(-.5))/(smhm.E(z)*h100)
    if(Mc < Mc_min) : 
        Mc = Mc_min
    fin = 1/((1+(Mc/Mh))**3)
    
    if(fstar+fgas > fin): 
        fgas = fin-fstar
        Mg = fgas*fb*Mh
    return Mg


def formationInfo2(case, minmass = 0, maxmass = 15, f = '/Users/nchoksi/gdrive/Astro/GC/Model/cat_opt.txt', nb = 10):
    
    p50, p25_blue, p50_blue, p75_blue, p25_red, p50_red, p75_red, logmh_list, logmh_list_red, logmh_list_blue = [], [], [], [], [], [], [], [], [], []
    
    hid, logmh, logsm, logmh_tform, logsm_tform, logm, logm_tform, zform, met  = np.loadtxt(f, unpack = True, usecols = (0,1,2,3,4,5,6,7,8))

       
    uid = np.unique(hid)
    for hv in uid:
        c = hid == hv
        logmh_hv = logmh[c]
        zform_hv = zform[c]
        logm_hv = logm[c]
        logmh_tform_hv = logmh_tform[c]
        logsm_tform_hv = logsm_tform[c]
        met_hv = met[c]

        b = (logm_hv > minmass) & (logm_hv < maxmass)
        logmh_hv = logmh_hv[b]
        zform_hv = zform_hv[b]
        logmh_tform_hv = logmh_tform_hv[b]
        logsm_tform_hv = logsm_tform_hv[b]
        met_hv = met_hv[b]
        logm_hv = logm_hv[b]

        if(case == 'zform'):
            arr = smhm.cosmicTime(zform_hv, units = 'Gyr')
        elif(case == 'mhtform'):
            arr = logmh_tform_hv
        elif(case == 'sm'):
            arr = np.log10(np.array([smhm.SMHM(10**m, zv, scatter = False) for m, zv in zip(logmh_tform_hv, zform_hv)]))
        elif(case == 'gas'):
            mg = []
            for i in range(len(logsm_tform_hv)):
                mg.append(gasMass(10**logsm_tform_hv[i],10**logmh_tform_hv[i], zform_hv[i]))
            arr = np.log10(mg)
        if(len(arr) <= 5):
            continue

        p50.append(np.percentile(arr, 50))
        logmh_list.append(logmh_hv[0])

        eqv = -0.9
        arr_blue = arr[met_hv  < eqv];
        arr_red = arr[met_hv >= eqv]  
        if(len(arr_blue) >= 2):
            p25_blue.append(np.percentile(arr_blue,25)); p50_blue.append(np.percentile(arr_blue, 50)); p75_blue.append(np.percentile(arr_blue,75));
            logmh_list_blue.append(logmh_hv[0])

        if(len(arr_red) >= 2): 
            p25_red.append(np.percentile(arr_red, 25)); p50_red.append(np.percentile(arr_red, 50)); p75_red.append(np.percentile(arr_red, 75))
            logmh_list_red.append(logmh_hv[0])

    p25_blue = np.array(p25_blue); p50_blue = np.array(p50_blue); p75_blue = np.array(p75_blue);
    p25_red = np.array(p25_red); p50_red = np.array(p50_red); p75_red = np.array(p75_red); p50 = np.array(p50); 
    logmh_list_red = np.array(logmh_list_red); logmh_list_blue = np.array(logmh_list_blue); logmh_list = np.array(logmh_list); 


    #get the medians of the 25th/50th/75th percentiles of the galaxies in the bin
    logmh_bins_blue, p25_blue_bins = binData(logmh_list_blue, p25_blue, numbins =nb, p = 50, add = True, eqn = False) 
    logmh_bins_blue, p50_blue_bins = binData(logmh_list_blue, p50_blue, numbins = nb, p = 50, add = True, eqn = False)
    logmh_bins_blue, p75_blue_bins = binData(logmh_list_blue, p75_blue, numbins = nb, p = 50, add = True, eqn = False)


    logmh_bins_red, p25_red_bins = binData(logmh_list_red, p25_red, numbins = nb, p = 50, add = True, eqn = False)
    logmh_bins_red, p50_red_bins = binData(logmh_list_red, p50_red, numbins = nb, p = 50, add = True, eqn = False)
    logmh_bins_red, p75_red_bins = binData(logmh_list_red, p75_red, numbins = nb, p = 50, add = True, eqn = False)

    logmh_bins, p50_bins = binData(logmh_list, p50, numbins = nb, p = 50, add = True, eqn = False)



    return logmh_bins, logmh_bins_red, logmh_bins_blue, p50_bins, p25_blue_bins, p50_blue_bins, p75_blue_bins, p25_red_bins,p50_red_bins, p75_red_bins



#now compute the specific ratios for each metallicity 
#logmstar_cuts is the M* at the time each halo MPB crosses the corresponding [Fe/H] of met_cuts (-2.5, -2.0...0)
def getSN(mmin, mmax, catpath, snpath):

	f = catpath
	hid, log_mh, log_mgc, zform, feh = np.loadtxt(f, usecols = (0,1,5,7,8), unpack = True)

	f = snpath
	hid_cuts, logmstar_cuts, met_cuts, znow, zbefore  = np.loadtxt(f, usecols = (0,1,2, 7, 8 ), unpack = True)
	uid_cuts = np.unique(hid_cuts)  
	errcount, tot, k = 0, 0 , 0
	sn_list, met_list = [], [] 
	b, b2, g = 0, 0, 0
	for hv in uid_cuts: #for each galaxy
		logmh = log_mh[hid == hv][0]

		if(logmh < mmin or logmh > mmax):
		    continue
		    
		c = hid_cuts == hv
		met_bins = met_cuts[c]
		logmstar_bins = logmstar_cuts[c]
		z_bins_now = znow[c]
		z_bins_before = zbefore[c]

		sm_sum = 0
		i = 0
		minidx = i; maxidx = i+1
		while(maxidx < len(met_bins)):

			met_before = met_bins[minidx]; met_next = met_bins[maxidx];
			dv = np.absolute(met_next - met_before)


			log_mgc_hv = log_mgc[hid == hv]
			feh_hv = feh[hid == hv]        

			mgc_epoch = np.sum ( 10**log_mgc_hv[(feh_hv >= met_before) & (feh_hv < met_next)]  )
			sm_epoch = 10**logmstar_bins[maxidx] - 10**logmstar_bins[minidx]

			if(sm_epoch <= 0 or mgc_epoch <= 0 or sm_epoch - mgc_epoch <= 0):
			    maxidx += 1
			    continue
			else:
			    minidx = maxidx
			    maxidx += 1
			if(met_next - met_before > 0.3):
			    b2 += 1
			    continue 
			else:
			    g+=1
			sn = mgc_epoch/(sm_epoch -mgc_epoch)
			sn_list.append(sn)
			mv = .5*(met_before + met_next)
			met_list.append(.5*(met_before + met_next))
			i += 1 
	return np.array(met_list), np.array(sn_list)
