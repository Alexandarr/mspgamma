#script to fix (artificial) dips in halo mass from Illustris merger trees

import numpy as np
import h5py
import time
import os
#mainpath = '/Users/nchoksi/gdrive/Astro/GC/Model/'
#treedir = mainpath + "SublinkTrees/"
mainpath = '/u/home/nickchok/analytic_model/'
treedir  = mainpath + "Sublink_Large/"
class Halo:
    def __init__(self, m, hid, descid, fp, mpi, z, spin):
        self.m = m
        self.hid = hid
        self.descid = descid
        self.fp = fp
        self.mpi = mpi
        self.z = z
        self.spin = spin

def loadTree(fname, mpb_only = False, reverse = True):

    f_one = h5py.File(treedir+fname, 'r')
    m = np.array(f_one['SubhaloMass'])*1e10/.704 #just mass_dm
    fp = (np.array(f_one['FirstProgenitorID'], dtype = long))
    subid = (np.array(f_one['SubhaloID'], dtype = long))
    snapshots = np.array(f_one['SnapNum']).astype(int)
    mpi = (np.array(f_one['MainLeafProgenitorID']))
    descid = np.array(f_one['DescendantID'], dtype = long)
    spin = np.array(f_one['SubhaloSpin'])

    c = m > 1e9
    spin = spin[c]; fp = fp[c]; subid = subid[c]; descid = descid[c]; snapshots = snapshots[c]; mpi = mpi[c]; m = m[c];
        
    c2 = fp != -1
    spin = spin[c2]; subid = subid[c2]; descid = descid[c2]; snapshots = snapshots[c2]; mpi = mpi[c2]; m = m[c2]; fp = fp[c2]
    
    #redshifts = np.array([getRedshift(s) for s in snapshots]) 
    redshifts = getRedshift(snapshots)
    mpbi = mpi[snapshots == np.amax(snapshots)][0] #the main progenitor branch id 
    msub = m[snapshots == np.amax(snapshots)][0]
    if(mpb_only):
        c3 = mpi == mpbi
        spin = spin[c3]; m = m[c3]; fp = fp[c3]; subid = subid[c3]; snapshots = snapshots[c3]; redshifts = redshifts[c3]; mpi = mpi[c3]
    return m[::-1], fp[::-1], subid[::-1], descid[::-1], snapshots[::-1], mpi[::-1], redshifts[::-1], spin[::-1], mpbi, msub
f = mainpath + 'snaps.txt'
snaps2 = np.loadtxt(f, unpack = True)
def getRedshift(sn):
    return snaps2[sn]
def load_id_dict():
    #f = '/users/nchoksi/gdrive/astro/gc/model/id_lookup2.txt'
    f = mainpath + "id_lookup_large.txt"
    fname_to_hid, hid_to_fname = {}, {}
    with open(f) as fi:
        for line in fi:
            hid = int(line[0:line.find(",")])
            fname = line[line.find(",")+1:-1]
            fname_to_hid[fname] = hid
            hid_to_fname[hid] = fname
    return fname_to_hid, hid_to_fname


fname_to_hid, hid_to_fname = load_id_dict()

outpath = mainpath + 'fixed_trees_large_spin/'
start = time.clock()

for fname in os.listdir(treedir): #for every z=0 halo
    if(fname == ".DS_Store"):
        continue
    hv = fname_to_hid[fname]
    m, fp, subid, descid, snapshots, mpi, redshifts, spin, mpbi, msub = loadTree(fname, mpb_only = False, reverse = True)
    halos = []
    for i in range(len(m)):
        halos.append(Halo(m[i], subid[i], descid[i], fp[i], mpi[i], redshifts[i], spin[i]))
    zu = np.unique(redshifts)[::-1]
    halos2 = []
    for mpiv in np.unique(mpi): #for every individual branch
        halos_mpi = [h2 for h2 in halos if h2.mpi == mpiv] #get all the halos along this branch
        halos_mpi.sort(key=lambda x: x.z, reverse=True) #sort in order of descending z 
        halos2.append(halos_mpi[0])
        zu = np.unique([h.z for h in halos_mpi])[::-1] #every redshift this branch exists at; each z will only have one halo;
        i = 0
        while (i <= len(zu)-1):
            znow = zu[i]
            halo_now = halos_mpi[i]
            j = i+1
            found = False
            while(not found and j < len(zu)): #search for the next halo along this branch that has delta M > 0 relative to halo_now (i.e., no mass decrease)
                znext = zu[j]
                desc = halos_mpi[j]
                if(desc.m < halo_now.m):
                    j += 1
                else:
                    halos2.append(Halo(desc.m, desc.hid, desc.descid, halo_now.hid, desc.mpi, desc.z, desc.spin)) #set fp of this halo to h_id of halo_now
                    found = True
            i = j
    m = [h.m for h in halos2]
    z = [h.z for h in halos2]
    fout = open(outpath + str(int(hv)) + ".txt", 'w')
    fout.write("logMh | fpID | subhaloID | main leaf ID | descID |  z\n")
    for h in halos2:
        fout.write(str(np.log10(h.m)) + " " + str(h.fp) + " " + str(h.hid) +  " " + str(h.mpi) + " " + str(h.descid) + " " + str(h.z) + " " + str(h.spin[0]) + " " + str(h.spin[1]) + " " + str(h.spin[2]) + "\n")
    fout.close()
end = time.clock()
print end-start
